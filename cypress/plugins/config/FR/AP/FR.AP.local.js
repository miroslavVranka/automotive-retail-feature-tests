// cypress/plugins/config/{AP|AC|DS}.json
module.exports = {
  env: {
    baseUrl: "http://localhost:3000",
    backofficeApiBaseURL:
      "https://psa-retail11-peugeot-qa.summit-automotive.solutions",
    modelNameplate: "/nouveau-suv-2008-suv",
    // with incluse options
    trimNameplate:
      "/allure/puretech-100-bvm6-essence/teinte-metallisee-orange-fusion/sellerie-tri-matiere-tissu-traxx-tep-surpiqures-bleu-virtuel",
    // with selected billed options
    carModel:
      "/c3-aircross-suv/shine/puretech-130-s-s-eat6-essence/breathing-blue-metallise/ambiance-metropolitan-grey/?options=YD01",
    language: "fr",
    priceFormat:
      "^([^0-9]*)((\\d{1,2}\\s\\d{1,3})|(\\d{1,3})|(\\d{1,3}(,)\\d{2})|(\\d{1,3}\\s\\d{3}(,)\\d{2}))\\s€(([^0-9]{0,4})|(\\/mois))$",
    px_vrm: "FC654BZ",
    divideSignCurrent: ",",
    divideSignExpected: ".",
    currencyLabelFormat: "TTC*",
    StorePriceIndex: "1_1",
    MonthlyPriceIndex: "3_1",
    DepositIndex: "2_1",
  },
};
