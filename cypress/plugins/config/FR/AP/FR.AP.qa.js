// cypress/plugins/config/{AP|AC|DS}.json
module.exports = {
  env: {
    baseUrl:
      "https://summit:summit@psa-retail11-peugeot-qa.summit-automotive.solutions",
    backofficeApiBaseURL:
      "https://psa-retail11-peugeot-qa.summit-automotive.solutions",
    modelNameplate: "/nouveau-suv-2008-suv",
    trimNameplate:
      "/gt-line/puretech-130-bvm6-essence/teinte-metallisee-orange-fusion/sellerie-tri-matiere-tissu-capy-tep-surpiqures-vert-adamite/",
    // with selected billed options
    carModel:
      "/nouveau-suv-2008-suv/gt-line/puretech-130-bvm6-essence/teinte-speciale-rouge-elixir/pack-cuir-nappa/",
    possibleModelIdx: [0, 1, 2, 3, 4, 5, 6, 7],
    language: "fr",
    priceFormat:
      "^([^0-9]*)((\\d{1,2}\\s\\d{1,3})|(\\d{1,3})|(\\d{1,3}(,)\\d{2})|(\\d{1,3}\\s\\d{3}(,)\\d{2}))\\s€(([^0-9]{0,4})|(\\/mois))$",
    px_vrm: "FC654BZ",
    divideSignCurrent: ",",
    divideSignExpected: ".",
    currencyLabelFormat: "TTC*",
    StorePriceIndex: "1_1",
    MonthlyPriceIndex: "3_1",
    DepositIndex: "2_1",
  },
};
