// cypress/plugins/config/{AP|AC|DS}.json
module.exports = {
  env: {
    baseUrl:
      "https://summit:summit@psa-retail11-peugeot-dev.summit-automotive.solutions",
    backofficeApiBaseURL:
      "https://psa-retail11-peugeot-dev.summit-automotive.solutions",
    modelNameplate: "/nouveau-suv-2008-suv",
    trimNameplate:
      "/gt-line/bluehdi-100-bvm6-diesel/teinte-opaque-noir-onyx/sellerie-tri-matiere-tissu-capy-tep-surpiqures-vert-adamite",
    // with selected billed options
    carModel:
      "/nouveau-suv-2008-suv/shine/puretech-110-s-s-bvm6-essence/gris-platinium-metallise/ambiance-metropolitan-grey/?options=YD01",
    possibleModelIdx: [0, 1],
    language: "fr",
    priceFormat:
      "^([^0-9]*)((\\d{1,2}\\s\\d{1,3})|(\\d{1,3})|(\\d{1,3}(,)\\d{2})|(\\d{1,3}\\s\\d{3}(,)\\d{2}))\\s€(([^0-9]{0,4})|(\\/mois))$",
    currencyLabelFormat: "TTC*",
    px_vrm: "FC654BZ",
    divideSignCurrent: ",",
    divideSignExpected: ".",
    StorePriceIndex: "1_1",
    MonthlyPriceIndex: "3_1",
    DepositIndex: "2_1",
  },
};
