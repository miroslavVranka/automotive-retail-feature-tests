// cypress/plugins/config/{AP|AC|DS}.js
module.exports = {
  env: {
    baseUrl: "http://localhost:3000",
    backofficeApiBaseURL:
      "https://psa-retail11-ds-qa.summit-automotive.solutions",
    modelNameplate: "/ds-3-crossback-suv",
    trimNameplate:
      "/chic/puretech-100-manuel-essence/blanc-banquise/inspiration-ds-montmartre",
    // with selected billed options
    arModel:
      "/ds-3-crossback-suv/grand-chic/bluehdi-130-automatique-diesel/or-imperial-metallisee/inspiration-ds-bastille-sieges-cuir-pavillon-fonce/",
    language: "fr",
    priceFormat:
      "^([^0-9]*)((\\d{1,2}\\s\\d{1,3})|(\\d{1,3})|(\\d{1,3}(,)\\d{2})|(\\d{1,3}\\s\\d{3}(,)\\d{2}))\\s€(([^0-9]{0,4})|(\\/mois))$",
    divideSignCurrent: ",",
    divideSignExpected: ".",
    currencyLabelFormat: "TTC*",
    px_vrm: "FC654BZ",
    StorePriceIndex: "1_1",
    MonthlyPriceIndex: "3_1",
    DepositIndex: "2_1",
  },
};
