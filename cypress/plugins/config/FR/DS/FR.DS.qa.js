// cypress/plugins/config/{AP|AC|DS}.js
module.exports = {
  env: {
    baseUrl:
      "https://summit:summit@psa-retail11-ds-qa.summit-automotive.solutions",
    backofficeApiBaseURL:
      "https://psa-retail11-ds-qa.summit-automotive.solutions",
    modelNameplate: "/ds-3-crossback-suv",
    trimNameplate:
      "/so-chic/bluehdi-100-manuel-diesel/blanc-banquise/inspiration-ds-bastille-avec-pavillon-clair",
    // with selected billed options
    carModel:
      "/so-chic/bluehdi-100-manuel-diesel/or-imperial-metallisee/inspiration-ds-bastille-avec-pavillon-clair/?options=NA01",
    possibleModelIdx: [0, 1],
    language: "fr",
    priceFormat:
      "^([^0-9]*)((\\d{1,2}\\s\\d{1,3})|(\\d{1,3})|(\\d{1,3}(,)\\d{2})|(\\d{1,3}\\s\\d{3}(,)\\d{2}))\\s€(([^0-9]{0,4})|(\\/mois))$",
    currencyLabelFormat: "TTC*",
    px_vrm: "FC654BZ",
    divideSignCurrent: ",",
    divideSignExpected: ".",
    StorePriceIndex: "1_1",
    MonthlyPriceIndex: "3_1",
    DepositIndex: "2_1",
  },
};
