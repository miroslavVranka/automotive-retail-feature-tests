// cypress/plugins/config/{AP|AC|DS}.js
module.exports = {
  env: {
    baseUrl:
      "https://summit:summit@psa-retail11-citroen-preprod.summit-automotive.solutions",
    backofficeApiBaseURL:
      "https://psa-retail11-citroen-preprod.summit-automotive.solutions",
    modelNameplate: "/c4-picasso-c4-spacetourer-grand-monospace/",
    // with incluse options
    trimNameplate:
      "/shine-pack/puretech-130-s-s-bvm6-essence/blanc-banquise/tissu-finn-black-cuir-grey-ambiance-hype-grey/",
    // with selected billed options
    carModel:
      "/nouveau-suv-c5-aircross-suv/feel/puretech-130-s-s-bvm6-essence/bleu-tijuca-metallisee/ambiance-metropolitan-grey",
    possibleModelIdx: [0, 1, 2, 3, 4, 5],
    language: "fr",
    priceFormat:
      "^([^0-9]*)((\\d{1,2}\\s\\d{1,3})|(\\d{1,3})|(\\d{1,3}(,)\\d{2})|(\\d{1,3}\\s\\d{3}(,)\\d{2}))\\s€(([^0-9]{0,4})|(\\/mois))$",
    px_vrm: "FC654BZ",
    divideSignCurrent: ",",
    divideSignExpected: ".",
    currencyLabelFormat: "TTC*",
    StorePriceIndex: "1_1",
    MonthlyPriceIndex: "3_1",
    DepositIndex: "2_1",
  },
};
