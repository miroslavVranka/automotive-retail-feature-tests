// cypress/plugins/config/{AP|AC|DS}.js
module.exports = {
  env: {
    baseUrl: "http://localhost:3000",
    backofficeApiBaseURL:
      "https://psa-retail11-citroen-qa.summit-automotive.solutions",
    modelNameplate: "/c3-aircross-suv",
    trimNameplate:
      "/feel/puretech-110-s-s-bvm6-essence/rouge-pepper-metallise/ambiance-serie/",
    // with selected billed options
    carModel:
      "/c3-aircross-suv/shine/puretech-110-s-s-bvm6-essence/gris-platinium-metallise/ambiance-metropolitan-grey/?options=YD01",
    language: "fr",
    priceFormat:
      "^([^0-9]*)((\\d{1,2}\\s\\d{1,3})|(\\d{1,3})|(\\d{1,3}(,)\\d{2})|(\\d{1,3}\\s\\d{3}(,)\\d{2}))\\s€(([^0-9]{0,4})|(\\/mois))$",
    currencyLabelFormat: "TTC*",
    px_vrm: "FC654BZ",
    divideSignCurrent: ",",
    divideSignExpected: ".",
    StorePriceIndex: "1_1",
    MonthlyPriceIndex: "3_1",
    DepositIndex: "2_1",
  },
};
