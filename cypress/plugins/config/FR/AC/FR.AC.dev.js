// cypress/plugins/config/{AP|AC|DS}.js
module.exports = {
  env: {
    baseUrl:
      "https://summit:summit@psa-retail11-citroen-dev.summit-automotive.solutions",
    backofficeApiBaseURL:
      "https://psa-retail11-citroen-dev.summit-automotive.solutions",
    modelNameplate: "/c3-aircross-suv",
    // with incluse options
    trimNameplate:
      "/feel/puretech-110-s-s-bvm6-essence/rouge-pepper-metallise/ambiance-serie/",
    // with selected billed options
    carModel:
      "/c4-picasso-c4-spacetourer-grand-monospace/shine/puretech-130-s-s-bvm6-essence/rouge-rubi-nacre/cuir-graine-black-ambiance-dune-beige/?options=WQ81",
    possibleModelIdx: [0],
    language: "fr",
    priceFormat:
      "^([^0-9]*)((\\d{1,2}\\s\\d{1,3})|(\\d{1,3})|(\\d{1,3}(,)\\d{2})|(\\d{1,3}\\s\\d{3}(,)\\d{2}))\\s€(([^0-9]{0,4})|(\\/mois))$",
    px_vrm: "FC654BZ",
    divideSignCurrent: ",",
    divideSignExpected: ".",
    currencyLabelFormat: "TTC*",
    StorePriceIndex: "1_1",
    MonthlyPriceIndex: "3_1",
    DepositIndex: "2_1",
  },
};
