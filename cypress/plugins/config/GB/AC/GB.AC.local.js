// cypress/plugins/config/{AP|AC|DS}.js
module.exports = {
  env: {
    baseUrl: "http://localhost:3000",
    backofficeApiBaseURL:
      "https://dev.citroen-uk-sol.psa-testing.summit-automotive.solutions",
    modelNameplate: "/new-c3-5-door",
    // with incluse options
    trimNameplate:
      "/flair/puretech-83-s-s-manual-petrol/soft-sand-metallic/grey-mica-cloth",
    // with selected billed options
    carModel:
      "/new-c3-5-door/flair/puretech-83-s-s-manual-petrol/cumulus-grey-metallic/techwood-interior-ambiance/",
    language: "en",
    priceFormat:
      "^([^0-9]*)£((\\d{1,3}(,)\\d{3})|(\\d{1,3}(,)\\d{3}.\\d{2})|(\\d{1,3}.\\d{2})|(\\d{1,3}))(([^0-9]{0,4})|(\\/month))$",
    divideSignCurrent: ",",
    divideSignExpected: "",
    currencyLabelFormatCash: "OTR",
    currencyLabelFormatFinance: "pm*",
    px_vrm: "LF15ZJO",
    StorePriceIndex: "1_1",
    MonthlyPriceIndex: "1_2",
    DepositIndex: "1_3",
  },
};
