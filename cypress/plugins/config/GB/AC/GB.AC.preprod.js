// cypress/plugins/config/{AP|AC|DS}.js
module.exports = {
  env: {
    baseUrl:
      "https://summit:summit@preprod.citroen-uk-sol.psa-testing.summit-automotive.solutions",
    backofficeApiBaseURL:
      "https://preprod.citroen-uk-sol.psa-testing.summit-automotive.solutions",
    modelNameplate: "/new-c3-5-door",
    trimNameplate:
      "/flair/puretech-83-s-s-manual-petrol/soft-sand-metallic/grey-mica-cloth/",
    // with selected billed options
    carModel:
      "/new-c3-5-door/flair/puretech-83-s-s-manual-petrol/spring-blue-metallic/techwood-interior-ambiance/",
    possibleModelIdx: [0, 1, 3],
    language: "en",
    priceFormat:
      "^([^0-9]*)£((\\d{1,3}(,)\\d{3})|(\\d{1,3}(,)\\d{3}.\\d{2})|(\\d{1,3}.\\d{2})|(\\d{1,3}))(([^0-9]{0,4})|(\\/month))$",
    divideSignCurrent: ",",
    divideSignExpected: "",
    currencyLabelFormatCash: "OTR",
    px_vrm: "LF15ZJO",
    currencyLabelFormatFinance: "pm*",
    StorePriceIndex: "1_1",
    MonthlyPriceIndex: "1_2",
    DepositIndex: "1_3",
  },
};
