/// <reference types="cypress" />
// ***********************************************************
// This example plugins/index.js can be used to load plugins
//
// You can change the location of this file or turn off loading
// the plugins file with the 'pluginsFile' configuration option.
//
// You can read more here:
// https://on.cypress.io/plugins-guide
// ***********************************************************

// This function is called when a project is opened or re-opened (e.g. due to
// the project's config changing)

/**
 * @type {Cypress.PluginConfig}
 */
const cucumber = require("cypress-cucumber-preprocessor").default;
const path = require("path");
const fs = require("fs-extra");
const allureWriter = require('@shelex/cypress-allure-plugin/writer');
const allure = require('allure-commandline');

// returns ChildProcess instance
const generation = allure(['generate', 'allure-results']);

// before(function () {
//     if (Cypress.env("COUNTRY") !== "GB") {
//         cy.getTestingData("Cash").then((data) => {
//             cy.writeFile("cypress/fixtures/customData/cash_urls.json", data);
//         });
//     }
//     cy.getTestingData("Finance").then((data) => {
//         cy.writeFile("cypress/fixtures/customData/finance_urls.json", data);
//     });
// });

module.exports = (on, config) => {
    // Run Cypress-Cucumber-Preprocessor
    on("file:preprocessor", cucumber());
    allureWriter(on, config);
    // more code and maybe return a config;
    const targetBrand = config.env.BRAND || "AP";
    const targetEnv = config.env.ENV || "local";
    const targetCountry = config.env.COUNTRY || "FR"
    return require(`./config/${targetCountry}/${targetBrand}/${targetCountry}.${targetBrand}.${targetEnv}`);
};
// after(function () {
//     generation.on('exit', function(exitCode) {
//         console.log('Generation is finished with code:', exitCode);
//     });
// });
