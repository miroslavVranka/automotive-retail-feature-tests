import {And, When, Given, Before} from 'cypress-cucumber-preprocessor/steps';
import {offers_cash_url, offers_finance_url} from "../constants/routs";


Given('I jump into offers cash page', () => {
    cy.navigateWithCookies(offers_cash_url);
})

Given('I jump into offers finance page', () => {
    cy.navigateWithCookies(offers_finance_url);
})

Given('I jump into offers finance trim page', () => {
    cy.getTestingData("B2C_Finance").then(td => {
        cy.navigateWithCookies(td.trimPageUrl);
    })
})

Before(() => {
    if (Cypress.env("COUNTRY") !== "GB") {
        cy.getTestingData("Cash").then((data) => {
            cy.writeFile("cypress/fixtures/customData/cash_urls.json", data);
        });
    }
    cy.getTestingData("Finance").then((data) => {
        cy.writeFile("cypress/fixtures/customData/finance_urls.json", data);
    });
})