import {And, When, Given, Then} from 'cypress-cucumber-preprocessor/steps';
import {offers_cash_url, offers_finance_url} from "../constants/routs";
import {Filter} from "../../page-objects/common/filter.js";
import {Models} from "../../page-objects/cars/models.js";
import {Trims} from "../../page-objects/cars/trims.js";
import {FinanceInfo} from "../../page-objects/configurator/financePanel";


Then('Url contains {string}', (character) => {
    Filter.urlContains(character);
})

Then("journey selector contains {string}", (expectedString) => {
    Filter.getSelectedJourney().should('contain', expectedString);
})

When('I filter car by {int} nth {string}', (nth, filterType) => {
    if (filterType == 'Energy') {
        Filter.filterByAnyEnergy(nth);
    } else {
        Filter.filterByAnyGearbox(nth);
    }
})

When('I filter car by price', (dataTable) => {
    const table = dataTable.hashes();
    table.forEach(row => {
            cy.navigateWithCookies(`${offers_finance_url}?prices=${row.minPrice}%2C${row.maxPrice}`)
        }
    );
})

Then('number of filtered models is greater than {int}', (modelsNumber) => {
    Models.getCount().should("be.gt", modelsNumber)
})

And('price of {int} nth model is greater than {int}', (modelNumber, modelPrice) => {
    Models.getPrices().getPrice(modelNumber).should("be.at.least", modelPrice)
})

And('deposit of {int} nth model is greater than {int}', (modelNumber, depositPrice) => {
    Models.getPrices().getDeposit(modelNumber).should("be.at.least", depositPrice)
})

Then('select {int} car from dropdown menu', (nth) => {
    Models.selectNthCar(nth)
});

Then('select {int} option on trim page', (nth) => {
    Trims.select(nth)
});

Then('url contain string {string}', (url) => {
    cy.url().should('contain', url);
});

When(/^switch into different journey$/, function () {
    Filter.clickOnJourneySelector();
});

When('open finance info option of {int} nth model from offered options', function (nth) {
    Models.getPrices().openFinanceInfo(nth)
});

Then('close finance info', function () {
    FinanceInfo.close();
});

When('start server for api validation', function (dataTable) {
    const table = dataTable.hashes();
    table.forEach(row => {
            cy.server();
            cy.route(row.method,
                `**${row.partialUrl}`
            ).as(row.alias);
        }
    );
});

Then(/^legal text (should|should not) exists$/, function (textExists) {
    let result = (textExists === 'should') ? 'exist' : 'not.exist'
    Models.getPrices().getLegalTexts().should(result);
});

Then('verify resultCode of api with alias {string}', function (alias) {
    cy.wait(`@${alias}`).should("have.property", "status", 200);
});
