import {
    car_details_list,
    car_features_list,
    car_nameplates,
    car_options_list,
} from '../../fixtures/api/api_variables';
import {Helpers} from '../helpers';

//Achtung: Please do not run prettier in this file!!!

Cypress.Commands.add('carDetailsRequest', (carNameplate, journeyType) => {
    return cy.request({
        method: car_details_list.method,
        url: car_details_list.url,
        header: car_details_list.headers,
        body: {
            "aggregationParams": {
                "levelAggregations": [
                    {
                        "name": "detailsAggregated",
                        "nesting": [
                            "detailsAggregated"
                        ],
                        "children": []
                    }
                ],
                "relevancyAggregations": [
                    {
                        "name": "prices.basePrice",
                        "fields": [
                            "exteriorColour",
                            "exteriorColourSlug",
                            "engine",
                            "specPackSlug",
                            "engineGearboxFuelSlug",
                            "gearbox",
                            "interiorColour",
                            "interiorColourSlug",
                            "prices",
                            "nameplateBodyStyleSlug",
                            "externalId"
                        ],
                        "parent": "detailsAggregated",
                        "operation": {
                            "size": 1,
                            "sort": "recommended"
                        }
                    }
                ]
            },
            "filters": [
                {
                    "nesting": [
                        "nameplateBodyStyleSlug"
                    ],
                    "name": "nameplateBodyStyleSlug",
                    "operator": "EQUALS",
                    "value": carNameplate,
                    "parent": null
                },
                {
                    "nesting": [
                        "prices",
                        "monthlyPrices",
                        "amount"
                    ],
                    "name": "prices.monthlyPrices.amount.global",
                    "operator": "BETWEEN",
                    "value": {
                        "from": 1,
                        "to": 99999
                    },
                    "parent": null
                },
                {
                    "nesting": [
                        "prices",
                        "type"
                    ],
                    "name": "prices.type",
                    "operator": "EQUALS",
                    "value": "Employee"
                },
                {
                    "nesting": [
                        "stock"
                    ],
                    "name": "stock",
                    "operator": "EQUALS",
                    "value": "false"
                }
            ],
            "extra": {
                "journey": journeyType.toLowerCase()
            }
        }
    });
});

Cypress.Commands.add('carDetailItemRequest', (externalId, journeyType, trimPageUrl) => {
    return cy.request({
        method: car_details_list.method,
        url: car_details_list.url,
        header: {
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0',
            'Accept': 'application/json',
            'Accept-Language': 'en-US,en;q=0.5',
            'Content-Type': 'application/json',
            'X-Auth-Token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJqd3QtaXNzdWVyIiwic3ViIjoic3NtLXVzZXItYXV0aCIsIm5iZiI6MTU5NTg0ODA2NCwiY3VzdG9tZXJfaWQiOm51bGwsInNlc3Npb25faWQiOiI1ZDcyOTc5Mi1jZmY5LTExZWEtYWVmMS0xODA1Y2YwMDkzZTAiLCJpYXQiOjE1OTU4NDgwNjQsImp0aSI6IjVkNzI5NzkyLWNmZjktMTFlYS1hZWYxLTE4MDVjZjAwOTNlMCJ9.PHIxJEQRkOWsObLyT2ppcgZ0_XVKMeJa-N7QKBC-dDE',
            'Origin': Cypress.env("backofficeApiBaseURL"),
            'Authorization': 'Basic c3VtbWl0OnN1bW1pdA==',
            'Connection': 'keep-alive',
            'Referer': trimPageUrl,
            'Cookie': '__cfduid=d974fc8c4f7d9fe66b457b10c4bbee0c21594974320; toky_state=minimized; SAUT_SESSION_DS_qa=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJqd3QtaXNzdWVyIiwic3ViIjoic3NtLXVzZXItYXV0aCIsIm5iZiI6MTU5NTc5OTc1OSwiY3VzdG9tZXJfaWQiOm51bGwsInNlc3Npb25faWQiOiJlNTVjNzIzOC1jZjg4LTExZWEtYWVmMS0xODA1Y2YwMDkzZTAiLCJpYXQiOjE1OTU3OTk3NTksImp0aSI6ImU1NWM3MjM4LWNmODgtMTFlYS1hZWYxLTE4MDVjZjAwOTNlMCJ9.U8zC2gmnETGsmzGcSRH_wfniuR5I4_iOqr0Jr9bJE2Y; _ga=GA1.2.1412767318.1594974331; _gcl_au=1.1.1822209919.1594974332; _fbp=fb.1.1594974333419.362924355; SAUT_SESSION_AP_qa=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJqd3QtaXNzdWVyIiwic3ViIjoic3NtLXVzZXItYXV0aCIsIm5iZiI6MTU5NTgwMDMyOSwiY3VzdG9tZXJfaWQiOm51bGwsInNlc3Npb25faWQiOiIzOGVkYjEzOC1jZjhhLTExZWEtYWVmMS0xODA1Y2YwMDkzZTAiLCJpYXQiOjE1OTU4MDAzMjksImp0aSI6IjM4ZWRiMTM4LWNmOGEtMTFlYS1hZWYxLTE4MDVjZjAwOTNlMCJ9.xFnFdB7jlO3OXh6gNlASTDti4dR9i5jnBsJlxdo9FOU; SAUT_SESSION_AP_preprod=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJqd3QtaXNzdWVyIiwic3ViIjoic3NtLXVzZXItYXV0aCIsIm5iZiI6MTU5NTM0MDEyNCwiY3VzdG9tZXJfaWQiOm51bGwsInNlc3Npb25faWQiOiJiYzIxMmU4ZS1jYjVhLTExZWEtYWVmMS0xODA1Y2YwMDkzZTAiLCJpYXQiOjE1OTUzNDAxMjQsImp0aSI6ImJjMjEyZThlLWNiNWEtMTFlYS1hZWYxLTE4MDVjZjAwOTNlMCJ9.HFO97-A9Ie6Cj9MfANy4mhZsDhu5JTRElK7O0Efakdg; _dy_ses_load_seq=33415%3A1595340530871; _dy_csc_ses=t; _dy_c_exps=; _dy_soct=1019478.1034049.1595340530; _dycnst=dg; _dyid=-3494792433070836384; _dyfs=1595340532278; _dyjsession=88c126f7991e8ad137ae6698ced9ecca; dy_fs_page=psa-retail11-peugeot-preprod.summit-automotive.solutions%2Ftrim%2Fconfigurable%2Ffinance%2Fnouvelle-208-5-portes; _dy_lu_ses=88c126f7991e8ad137ae6698ced9ecca%3A1595340532279; _dycst=dk.w.f.ss.; _dy_geo=CZ.EU.CZ_10.CZ_10_Prague; _dy_df_geo=Czech%20Republic..Prague; _dy_toffset=-1; _tag_frontend_15355_vid=08b389773cbe7c7af69bfa8c477fbd220cd75b120b7cabd5%3A6eb4; _psac_gdpr_banner_id=0; _psac_gdpr_consent_purposes=[cat_ana][cat_com][cat_soc]; _psac_gdpr_consent_cookies=[Google Tag Manager][4w MarketPlace][Adara][Emetriq][IginitionOne (Netmining)][Smart Adserver][Xaxis][Adadyn (formerly Ozone Media)][Adyoulike][AppNexus][mPlatform][Netmining][Plista][Quantcast][Facebook]; _ga=GA1.3.1412767318.1594974331; _psac_gdpr_consent_purposes_opposition=; _psac_gdpr_consent_given=1; ivbspd=4; SAUT_SESSION_AC_qa=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJqd3QtaXNzdWVyIiwic3ViIjoic3NtLXVzZXItYXV0aCIsIm5iZiI6MTU5NTg0ODA2NCwiY3VzdG9tZXJfaWQiOm51bGwsInNlc3Npb25faWQiOiI1ZDcyOTc5Mi1jZmY5LTExZWEtYWVmMS0xODA1Y2YwMDkzZTAiLCJpYXQiOjE1OTU4NDgwNjQsImp0aSI6IjVkNzI5NzkyLWNmZjktMTFlYS1hZWYxLTE4MDVjZjAwOTNlMCJ9.PHIxJEQRkOWsObLyT2ppcgZ0_XVKMeJa-N7QKBC-dDE; SAUT_SESSION_DS_preprod=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJqd3QtaXNzdWVyIiwic3ViIjoic3NtLXVzZXItYXV0aCIsIm5iZiI6MTU5NTM0MDQ2MiwiY3VzdG9tZXJfaWQiOm51bGwsInNlc3Npb25faWQiOiI4NWE5NjA2Ny1jYjViLTExZWEtYWVmMS0xODA1Y2YwMDkzZTAiLCJpYXQiOjE1OTUzNDA0NjIsImp0aSI6Ijg1YTk2MDY3LWNiNWItMTFlYS1hZWYxLTE4MDVjZjAwOTNlMCJ9.sW6AIr638C17qLLb0FXwXu-cgqRLGOs8inuDmYhpagE; _tag_frontend_82b15_vid=424de23309b27d530f31793933c0467c2d53bf0323f6abac%3A5858; SAUT_SESSION_AC_preprod=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJqd3QtaXNzdWVyIiwic3ViIjoic3NtLXVzZXItYXV0aCIsIm5iZiI6MTU5NTM0MDU0NiwiY3VzdG9tZXJfaWQiOm51bGwsInNlc3Npb25faWQiOiJiNzc2ZjZhZC1jYjViLTExZWEtYWVmMS0xODA1Y2YwMDkzZTAiLCJpYXQiOjE1OTUzNDA1NDYsImp0aSI6ImI3NzZmNmFkLWNiNWItMTFlYS1hZWYxLTE4MDVjZjAwOTNlMCJ9.Q8NGb32JfKcTfN1RrEZ9x5HD7gU43z4jQNaEBei1asE; _tag_frontend_42071_vid=4c559ead53a6d2f1b94655139b28016d6e9d1576f9d5edcf6c%3A99ea; cto_bundle=f_IOHl9Fc3ZyekxLcWR0ZGwlMkYyU0olMkZCTWpySnZhNiUyRjVNdkhDd0ZmcDExSklZSE1ZaWJjYkFWeFlwaUNnUk0lMkY4QkJVUzRFTnlNRmxNSkRXYjNzb2EzVmZacHJBTVVVb1QwVm9td2Y1SURYZEVzZ0Q4c2lRSTJDM3djbkslMkJPZTBwWjlhWmU1QWNBSlAlMkZqMVFtQUclMkZJYUFSYVhJbGthVEU4a2VQSUVZTUdlaldVZHBKTUNUMDJuJTJCeGR1MngzeWVsUG54ZyUyQmdWRHNaNFY4TGFKVERIQjZpSDF1em9RJTNEJTNE; _gid=GA1.2.1736441961.1595799765; _gid=GA1.3.1736441961.1595799765; _uetsid=21c2901ab157ab3f04bc7f5ad70bc3c0; _uetvid=34f9a87bbfbb346e4e4840069eecd020; _gali=TESTING_INTERIOR_2; _gali=TESTING_INTERIOR_2; _dc_gtm_UA-46309021-1=1; _dc_gtm_UA-45190795-1=1; _gat_UA-46309021-1=1; _gat_UA-45190795-1=1; SAUT_SESSION_AC_qa=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJqd3QtaXNzdWVyIiwic3ViIjoic3NtLXVzZXItYXV0aCIsIm5iZiI6MTU5NjIzMjAwOSwiY3VzdG9tZXJfaWQiOm51bGwsInNlc3Npb25faWQiOiI0YzIxMmY2Ny1kMzc3LTExZWEtYWVmMS0xODA1Y2YwMDkzZTAiLCJpYXQiOjE1OTYyMzIwMDksImp0aSI6IjRjMjEyZjY3LWQzNzctMTFlYS1hZWYxLTE4MDVjZjAwOTNlMCJ9._ft7W3Uucs2n7ky9QqdiCVZYaVPTXqkpENm3SXVrT48',
            'TE': 'Trailers'
        },
        body: {
            "aggregationParams": {
                "levelAggregations": [{
                    "name": "detailsOptionsAggregated",
                    "nesting": ["detailsOptionsAggregated"],
                    "children": []
                }],
                "relevancyAggregations": [{
                    "name": "prices.monthlyPrices.amount",
                    "fields": ["*"],
                    "parent": "detailsOptionsAggregated",
                    "operation": {"size": 1, "sort": "asc"}
                }]
            },
            "filters": [{
                "nesting": ["externalId"],
                "name": "externalId",
                "operator": "EQUALS",
                "value": externalId
            }, {"nesting": ["stock"], "name": "stock", "operator": "EQUALS", "value": "false"}],
            "extra": {"journey": journeyType.toLowerCase()}
        }
    });
});

Cypress.Commands.add('carNameplatesRequest', (journeyType) => {
    return cy.request({
        method: car_nameplates.method,
        url: car_nameplates.url,
        header: car_nameplates.headers,
        body: {
            "aggregationParams": {
                "levelAggregations": [{
                    "name": "nameplateBodyStyle",
                    "nesting": ["nameplateBodyStyle"],
                    "children": []
                }],
                "relevancyAggregations": [{
                    "name": "prices.monthlyPrices.amount",
                    "fields": ["id", "model", "prices", "images", "lcdv16", "bodyStyle", "nameplate", "grGearbox", "grBodyStyle", "nameplateBodyStyle", "promotionalText", "pricesV2", "regularPaymentConditions", "noDeposit", "offers", "specPack", "nameplateBodyStyleSlug", "exteriorColourSlug", "interiorColourSlug", "specPackSlug", "engineGearboxFuelSlug", "interiorColour", "exteriorColour", "engine", "fuel", "gearbox", "title", "isDefaultConfiguration", "defaultConfiguration", "externalId"],
                    "parent": "nameplateBodyStyle",
                    "operation": {"size": 1, "sort": "asc"}
                }]
            },
            "filters": [{
                "nesting": ["prices", "type"],
                "name": "prices.basePrice",
                "operator": "EQUALS",
                "value": "Employee"
            }, {
                "nesting": ["prices", "type"],
                "name": "prices.type",
                "operator": "EQUALS",
                "value": "Employee"
            }, {"nesting": ["stock"], "name": "stock", "operator": "EQUALS", "value": "false"}],
            "extra": {"journey": journeyType.toLowerCase()}
        }
    });
});

Cypress.Commands.add('carOptionsListRequest', (externalId, journeyType, referUrl) => {
    return cy.request({
        method: car_options_list.method,
        url: car_options_list.url,
        header: {
            'Connection': 'keep-alive',
            'Accept': 'application/json',
            'X-Auth-Token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJqd3QtaXNzdWVyIiwic3ViIjoic3NtLXVzZXItYXV0aCIsIm5iZiI6MTU5NjYzNTI1NSwiY3VzdG9tZXJfaWQiOm51bGwsInNlc3Npb25faWQiOiJkM2ZlNmMyNi1kNzIxLTExZWEtOTBlZC0zMGQ0OTY3NDgyMzIiLCJpYXQiOjE1OTY2MzUyNTUsImp0aSI6ImQzZmU2YzI2LWQ3MjEtMTFlYS05MGVkLTMwZDQ5Njc0ODIzMiJ9.CN4UnvCDt3uYz-gJ0KZYrxxb8x7rcMs18Hv_j6gTNwc',
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.105 Safari/537.36',
            'Content-Type': 'application/json',
            'Sec-Fetch-Site': 'same-origin',
            'Sec-Fetch-Mode': 'cors',
            'Sec-Fetch-Dest': 'empty',
            'Referer': referUrl,
            'Accept-Language': 'en-US,en;q=0.9',
            'Cookie': '_psac_gdpr_consent_purposes=[cat_ana]; _psac_gdpr_consent_given=1; __cfduid=d43baea9b3c2a82a35c56d4faa7646bd61596635243; SAUT_SESSION_AC_dev=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJqd3QtaXNzdWVyIiwic3ViIjoic3NtLXVzZXItYXV0aCIsIm5iZiI6MTU5NjYzNTI1NSwiY3VzdG9tZXJfaWQiOm51bGwsInNlc3Npb25faWQiOiJkM2ZlNmMyNi1kNzIxLTExZWEtOTBlZC0zMGQ0OTY3NDgyMzIiLCJpYXQiOjE1OTY2MzUyNTUsImp0aSI6ImQzZmU2YzI2LWQ3MjEtMTFlYS05MGVkLTMwZDQ5Njc0ODIzMiJ9.CN4UnvCDt3uYz-gJ0KZYrxxb8x7rcMs18Hv_j6gTNwc; _psac_gdpr_banner_id=0; _psac_gdpr_consent_cookies=[Google Tag Manager]; _ga=GA1.2.744927128.1596635260; _gid=GA1.2.1164423030.1596635260; _ga=GA1.5.744927128.1596635260; _gid=GA1.5.1164423030.1596635260; SAUT_SESSION_AC_qa=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJqd3QtaXNzdWVyIiwic3ViIjoic3NtLXVzZXItYXV0aCIsIm5iZiI6MTU5NjIzMjAwOSwiY3VzdG9tZXJfaWQiOm51bGwsInNlc3Npb25faWQiOiI0YzIxMmY2Ny1kMzc3LTExZWEtYWVmMS0xODA1Y2YwMDkzZTAiLCJpYXQiOjE1OTYyMzIwMDksImp0aSI6IjRjMjEyZjY3LWQzNzctMTFlYS1hZWYxLTE4MDVjZjAwOTNlMCJ9._ft7W3Uucs2n7ky9QqdiCVZYaVPTXqkpENm3SXVrT48'
        },
        body: {
            "filters": [{"nesting": ["stock"], "name": "stock", "operator": "EQUALS", "value": "false"}],
            "extra": {"externalId": externalId, "journey": journeyType.toLowerCase(), "optionIds": []}
        }
    });
});
