import {
    car_details_list,
    car_features_list,
    car_nameplates,
    car_options_list,
} from '../../fixtures/api/api_variables';
import {Helpers} from '../helpers';
import {offers_finance_refer_url} from "../constants/routs";

//Achtung: Please do not run prettier in this file!!!

Cypress.Commands.add('postCarDetailsAggregated', (carNameplate, journeyType, carWithDefaultConfiguration) => {
    cy.carDetailsRequest(carNameplate, journeyType).as('cda')
    cy.get("@cda").should("have.property", "status", 200)
    cy.get("@cda").then((response) => {
        let carData = [];
        let carItems = response.body.items[0].items;
        carItems.forEach(function (item) {
            let basePrice = item.items['prices.basePrice']
            basePrice.forEach(function (bp) {
                let exteriorColors = bp.exteriorColour.pricesV2
                exteriorColors.forEach(function (ec) {
                    let interiorColour = bp.interiorColour.pricesV2
                    interiorColour.forEach(function (ic) {
                        if (ec.type === `B2C_${journeyType}` && ec.finalPriceInclTax > 0 && ic.type === `B2C_${journeyType}`
                            && ic.finalPriceInclTax > 0 && carWithDefaultConfiguration === false) {
                            carData.push({
                                "externalId": bp.externalId,
                                "nameplateBodyStyleSlug": bp.nameplateBodyStyleSlug,
                                "specPackSlug": bp.specPackSlug,
                                "engineGearboxFuelSlug": bp.engineGearboxFuelSlug,
                                "exteriorColourSlug": bp.exteriorColourSlug,
                                "exteriorColourFinalPriceInclTax": ec.finalPriceInclTax,
                                "interiorColourSlug": bp.interiorColourSlug,
                                "interiorColourFinalPriceInclTax": ic.finalPriceInclTax
                            })
                        }
                        if (ec.type === `B2C_${journeyType}` && ec.finalPriceInclTax === 0 && ic.type === `B2C_${journeyType}`
                            && ic.finalPriceInclTax === 0 && carWithDefaultConfiguration) {
                            carData.push({
                                "externalId": bp.externalId,
                                "nameplateBodyStyleSlug": bp.nameplateBodyStyleSlug,
                                "specPackSlug": bp.specPackSlug,
                                "engineGearboxFuelSlug": bp.engineGearboxFuelSlug,
                                "exteriorColourSlug": bp.exteriorColourSlug,
                                "exteriorColourFinalPriceInclTax": ec.finalPriceInclTax,
                                "interiorColourSlug": bp.interiorColourSlug,
                                "interiorColourFinalPriceInclTax": ic.finalPriceInclTax
                            })
                        }
                    });
                });
            });
        });
        return carData[Math.floor(Math.random() * carData.length)];
    });
});

Cypress.Commands.add('postCarDetailsDataCheck', (carNameplate, externalId ,journeyType, carWithDefaultConfiguration) => {
    try {
    cy.carDetailsRequest(carNameplate, journeyType).then(response => {
        if (response.status === 200 ){
            let carData = [];
            let carItems = response.body.items[0].items;
            carItems.map(function (item) {
                let basePrice = item.items['prices.basePrice']
                basePrice.map(function (bp) {
                    let exteriorColors = bp.exteriorColour.pricesV2
                    exteriorColors.map(function (ec) {
                        let interiorColour = bp.interiorColour.pricesV2
                        if (interiorColour.length > 1) {
                            interiorColour.map(function (ic) {
                                if (ec.type === `B2C_${journeyType}` && ec.finalPriceInclTax > 0 && ic.type === `B2C_${journeyType}`
                                    && ic.finalPriceInclTax > 0 && interiorColour.length > 1 && carWithDefaultConfiguration === false) {
                                    carData.push({
                                        "externalId": bp.externalId,
                                        "externalIdFromNameplate": externalId,
                                        "nameplateBodyStyleSlug": bp.nameplateBodyStyleSlug,
                                        "specPackSlug": bp.specPackSlug,
                                        "engineGearboxFuelSlug": bp.engineGearboxFuelSlug,
                                        "exteriorColourSlug": bp.exteriorColourSlug,
                                        "exteriorColourFinalPriceInclTax": ec.finalPriceInclTax,
                                        "interiorColourSlug": bp.interiorColourSlug,
                                        "interiorColourFinalPriceInclTax": ic.finalPriceInclTax
                                    })
                                }
                                if (ec.type === `B2C_${journeyType}` && ec.finalPriceInclTax === 0 && ic.type === `B2C_${journeyType}`
                                    && ic.finalPriceInclTax === 0 && interiorColour.length > 1 && carWithDefaultConfiguration) {
                                    carData.push({
                                        "externalId": bp.externalId,
                                        "externalIdFromNameplate": externalId,
                                        "nameplateBodyStyleSlug": bp.nameplateBodyStyleSlug,
                                        "specPackSlug": bp.specPackSlug,
                                        "engineGearboxFuelSlug": bp.engineGearboxFuelSlug,
                                        "exteriorColourSlug": bp.exteriorColourSlug,
                                        "exteriorColourFinalPriceInclTax": ec.finalPriceInclTax,
                                        "interiorColourSlug": bp.interiorColourSlug,
                                        "interiorColourFinalPriceInclTax": ic.finalPriceInclTax
                                    })
                                }
                            });
                        }
                    });
                });
            });
            return carData[Math.floor(Math.random() * carData.length)];
        } else {
            // throw new Error(`There is no data for carNameplate ${carNameplate}`)
            console.log(`There is no data for carNameplate ${carNameplate}`)
        }
        });
    // });
    } catch (err) {
        console.log(err)
        return []
    }
});

Cypress.Commands.add('postCarDetailItem', (externalId, journeyType, trimPageUrl) => {
    cy.carDetailItemRequest(externalId, journeyType, trimPageUrl).as("cdi")
    cy.get("@cdi").should("have.property", "status", 200)
    cy.get("@cdi").then((response) => {
        let carItem = response.body.items[0].items[0].items["prices.monthlyPrices.amount"][0];
        let pricesList = carItem.extraFields.pricesV2;
        let results = null;
        pricesList.forEach((priceItem) => {
            if (priceItem.type === `Total_B2C_${journeyType}`) {
                results = priceItem;
            }
        });
        console.log(results)
        return {
            "nameplateBodyStyleSlug": carItem.nameplateBodyStyleSlug,
            "specPackSlug": carItem.specPackSlug,
            "engineGearboxFuelSlug": carItem.engineGearboxFuelSlug,
            "exteriorColourSlug": carItem.exteriorColourSlug,
            "interiorColourSlug": carItem.interiorColourSlug,
            "brandEStorePriceOnTheRoad": results.breakdown.brandEStorePriceOnTheRoad,
            "brandEStorePriceOnTheRoadWithPromoCode": results.breakdown.brandEStorePriceOnTheRoadWithPromoCode,
            "versionBrandEStorePriceOnTheRoadWithPromoCode": results.breakdown.versionBrandEStorePriceOnTheRoadWithPromoCode,
            "basePriceInclTax": results.basePriceInclTax,
            "optionsFinalPrice": results.breakdown.optionsFinalPrice
        }
    })
})

Cypress.Commands.add('postCarOptionsList', (externalId, journeyType, referUrl) => {
    cy.carOptionsListRequest(externalId, journeyType, referUrl).as("col")
    cy.get("@col").should("have.property", "status", 200)
    cy.get("@col").then((response) => {
        let options = response.body.items
        let possibleOptions = []
        options.forEach((opt) => {
            let prices2 = opt['pricesV2']
            prices2.forEach((prc) => {
                if (prc.type === `B2C_${journeyType}` && prc.finalPriceInclTax > 0) {
                    possibleOptions.push({
                        "optionId": opt.id,
                        "finalPriceInclTax": prc.finalPriceInclTax
                    });
                }
            });
        });
        return possibleOptions;
    });
});

Cypress.Commands.add('postCarNameplates', (journeyType) => {
    cy.carNameplatesRequest(journeyType).as('cnp')
    cy.get("@cnp").should("have.property", "status", 200)
    cy.get("@cnp").then((response) => {
        let carItems = response.body.items;
        let possibleOptions = [];
        carItems.map(carItem => {
            let car = carItem.items["prices.monthlyPrices.amount"][0]
            possibleOptions.push({
                "externalId": car.externalId,
                "nameplateBodyStyleSlug": car.nameplateBodyStyleSlug
            });
        });
        let possibleIdx = Helpers.randomItem(Cypress.env("possibleModelIdx"));
        return possibleOptions;
    })
})
Cypress.Commands.add('getValidData', (journeyType, carWithDefaultConfiguration) => {
    cy.postCarNameplates("Cash").then(nameplate => {
        let possibleOption = null
        nameplate.some(function(rangeValue) {
            cy.postCarDetailsDataCheck(rangeValue.nameplateBodyStyleSlug, rangeValue.externalId, journeyType, carWithDefaultConfiguration).as('data')
                possibleOption = cy.get('@data')
                return true
            })
    });
});