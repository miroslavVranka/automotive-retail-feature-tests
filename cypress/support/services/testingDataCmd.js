import {
    cash_part_url,
    config_part_url,
    finance_part_url,
    journey_offers_part_url,
    trim_part_url
} from "../constants/routs";


Cypress.Commands.add('getTestingData', (journeyType) => {

    let configPageReferPrefix = Cypress.env("backofficeApiBaseURL") + config_part_url + journey_offers_part_url + "/" + journeyType.toLowerCase();
    let configPageUrlPrefix = Cypress.env("baseUrl") + config_part_url + journey_offers_part_url + "/" + journeyType.toLowerCase();
    let trimPageUrlPrefix = Cypress.env("baseUrl") + trim_part_url + journey_offers_part_url + "/" + journeyType.toLowerCase();

        cy.getValidData(journeyType, true).then(cdad => {
            cy.getValidData(journeyType, false).then(cdac => {
                console.log(cdad)
                console.log(cdac)

                let trimPageUrl = trimPageUrlPrefix + "/" + cdac.nameplateBodyStyleSlug

                let configPageUrlBody = configPageUrlPrefix + "/" + cdad.nameplateBodyStyleSlug + "/" + cdad.specPackSlug
                    + "/" + cdad.engineGearboxFuelSlug + "/";
                let configPageUrlBodyCustom = configPageUrlPrefix + "/" + cdac.nameplateBodyStyleSlug + "/" + cdac.specPackSlug
                    + "/" + cdac.engineGearboxFuelSlug + "/";
                let configPageRefBody = configPageReferPrefix + "/" + cdac.nameplateBodyStyleSlug + "/" + cdac.specPackSlug
                    + "/" + cdac.engineGearboxFuelSlug + "/";

                let configPageUrlSuffixDefault = cdad.exteriorColourSlug + "/" + cdad.interiorColourSlug
                let configPageUrlSuffixCustom = cdac.exteriorColourSlug + "/" + cdac.interiorColourSlug
                let configPageRefCustom = configPageRefBody + configPageUrlSuffixCustom

                cy.postCarOptionsList(cdac.externalId, journeyType, trimPageUrl).then(col => {

                    let optionsSuffix = '?options=' + col[1].optionId + '%2C' + col[0].optionId;
                    let configPageWithOptionsCustom = configPageUrlBodyCustom + configPageUrlSuffixCustom + "/" + optionsSuffix
                    let configPageRefWithOpsCustom = configPageRefCustom + "/" + optionsSuffix
                    let externalIdWithOps = cdac.externalId + "+" + col[1].optionId + "+" + col[0].optionId;

                    return {
                    //dynamicUrls
                    "configPageUrl": configPageUrlBody + configPageUrlSuffixDefault,
                    "configPageUrlCustom": configPageUrlBodyCustom + configPageUrlSuffixCustom,
                    "configPageRefUrlCustom": configPageRefCustom,
                    "configPageWithOptionsCustom": configPageWithOptionsCustom,
                    "configPageRefWithOpsCustom": configPageRefWithOpsCustom,
                    "externalIdWithOps": externalIdWithOps,
                    "trimPageUrl": trimPageUrl,
                    "journeyType": journeyType,

                    "nameplateBodyStyleSlug": cdad.nameplateBodyStyleSlug,
                    "externalIdFromNamePlates": cdad.externalIdFromNameplate,

                    //getCarDetailsAggregated
                    "exteriorColourFinalPriceInclTax": cdac.exteriorColourFinalPriceInclTax,
                    "interiorColourFinalPriceInclTax": cdac.interiorColourFinalPriceInclTax,
                    "externalIdDefault": cdad.externalId,
                    "externalIdCustom": cdac.externalId,
                    //postCarOptionsList
                    "finalPriceInclTaxOp1": col[1].finalPriceInclTax,
                    "finalPriceInclTaxOp2": col[0].finalPriceInclTax
                }
            });
            });
        });
});

Cypress.Commands.add('getTestingDataWithOps', (journeyType) => {

    let configPageUrlPrefix = Cypress.env("baseUrl") + config_part_url + journey_offers_part_url + "/" + journeyType.toLowerCase();
    let trimPageUrlPrefix = Cypress.env("baseUrl") + trim_part_url + journey_offers_part_url + "/" + journeyType.toLowerCase();
    let configPageReferPrefix = Cypress.env("backofficeApiBaseURL") + config_part_url + journey_offers_part_url + "/" + journeyType.toLowerCase();

        cy.getValidData(journeyType, false).then(cdac => {
            console.log(cdac)
            let trimPageUrl = trimPageUrlPrefix + "/" + cdac.nameplateBodyStyleSlug

            let configPageUrlBodyCustom = configPageUrlPrefix + "/" + cdac.nameplateBodyStyleSlug + "/" + cdac.specPackSlug
                + "/" + cdac.engineGearboxFuelSlug + "/";
            let configPageUrlSuffixCustom = cdac.exteriorColourSlug + "/" + cdac.interiorColourSlug

            let configPageRefBody = configPageReferPrefix + "/" + cdac.nameplateBodyStyleSlug + "/" + cdac.specPackSlug
                + "/" + cdac.engineGearboxFuelSlug + "/";

            let configPageRefCustom = configPageRefBody + configPageUrlSuffixCustom

            cy.postCarOptionsList(cdac.externalId, journeyType, trimPageUrl).then(col => {
                console.log(col)
                let optionsSuffix = '?options=' + col[1].optionId + '%2C' + col[0].optionId;
                let configPageWithOptionsCustom = configPageUrlBodyCustom + configPageUrlSuffixCustom + "/" + optionsSuffix
                let configPageRefWithOpsCustom = configPageRefCustom + "/" + optionsSuffix
                let externalIdWithOps = cdac.externalId + "+" + col[1].optionId + "+" + col[0].optionId;
                return {
                    //dynamicUrls
                    "configPageUrlCustom": configPageUrlBodyCustom + configPageUrlSuffixCustom,
                    "configPageWithOptionsCustom": configPageWithOptionsCustom,
                    "configPageRefWithOpsCustom": configPageRefWithOpsCustom,
                    "configPageRefUrlCustom": configPageRefCustom,
                    "trimPageUrl": trimPageUrl,
                    "externalIdWithOps": externalIdWithOps,
                    "externalIdCustom": cdac.externalId,
                    "journeyType": journeyType,

                    //postCarNameplates
                    "nameplateBodyStyleSlug": cdac.nameplateBodyStyleSlug,
                    "externalIdFromNamePlates": cdac.externalIdFromNameplate,

                    "exteriorColourFinalPriceInclTax": cdac.exteriorColourFinalPriceInclTax,
                    "interiorColourFinalPriceInclTax": cdac.interiorColourFinalPriceInclTax,

                    //postCarOptionsList
                    "finalPriceInclTaxOp1": col[1].finalPriceInclTax,
                    "finalPriceInclTaxOp2": col[0].finalPriceInclTax

                }
            });
        });
});