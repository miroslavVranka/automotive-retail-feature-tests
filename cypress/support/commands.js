/// <reference types="Cypress" />

Cypress.Commands.add("navigateWithCookies", (url) => {
    cy.setCookie("_psac_gdpr_consent_given", "1");
    cy.visit(url);
    cy.wait(2000);
});

// Cypress.Commands.add("getActualModelNameplate", () => {
//   cy.request(
//     "POST",
//     request_with_basic_auth(Cypress.env("backofficeApiBaseURL")) +
//       get_namepalates_url(Cypress.env("brand"))
//   );
// });

Cypress.Commands.add("getByDataId", (dataId) => {
    cy.get(`[data-testid='${dataId}']`);
});

Cypress.Commands.add("getByDataIdWithChild", (dataId, child) => {
    cy.get(`[data-testid='${dataId}'] > ${child}`);
});

Cypress.Commands.add("getByDataIdWith2Childs", (dataId, child) => {
    cy.get(`[data-testid^='${dataId}'] > ${child} > ${child}`);
});

Cypress.Commands.add("getByContainsId", (dataId) => {
    cy.get(`[id^='${dataId}']`);
});

Cypress.Commands.add("getByContainsDataId", (dataId) => {
    cy.get(`[data-testid^='${dataId}']`);
});

Cypress.Commands.add("getById", (id) => {
    cy.get(`[id='${id}']`);
});

Cypress.Commands.add(
    "selectNth",
    { prevSubject: "element" },
    (subject, pos) => {
        cy.wrap(subject)
            .children("option")
            .eq(pos)
            .then((e) => {
                cy.wrap(subject).select(e.val());
            });
    }
);

Cypress.Commands.add("text", { prevSubject: true }, (subject, options) => {
    return subject.text();
});

Cypress.Commands.add(
    "getAttributes",
    {
        prevSubject: true,
    },
    (subject, attr) => {
        const attrList = [];
        cy.wrap(subject).each(($el) => {
            cy.wrap($el)
                .invoke("attr", attr)
                .then((lid) => {
                    attrList.push(lid);
                });
        });
        return cy.wrap(attrList);
    }
);

Cypress.Commands.add(
    "shouldHaveTrimmedText",
    { prevSubject: true },
    (subject, equalTo) => {
        expect(subject.text().replace(/ /g, "")).to.eq(
            equalTo.text().replace(/ /g, "")
        );
        return subject;
    }
);
