/// <reference types="Cypress" />

import {
  delete_all_faq_url,
  delete_one_faq_url,
  create_faq_url,
} from "./constants/spcBackend";

import { request_with_basic_auth } from "../support/constants/routs";

Cypress.Commands.add("deleteAllFAQs", () => {
  cy.request(
    "DELETE",
    request_with_basic_auth(Cypress.env("backofficeApiBaseURL")) +
      delete_all_faq_url
  );
});
Cypress.Commands.add("deleteFAQById", (id) => {
  cy.request(
    "DELETE",
    request_with_basic_auth(Cypress.env("backofficeApiBaseURL")) +
      delete_one_faq_url +
      id
  );
});
Cypress.Commands.add("addFAQItem", (item) => {
  cy.request({
    method: "POST",
    url:
      request_with_basic_auth(Cypress.env("backofficeApiBaseURL")) +
      create_faq_url,
    form: true,
    json: true,
    body: {
      data: JSON.stringify(item),
    },
  });
});
