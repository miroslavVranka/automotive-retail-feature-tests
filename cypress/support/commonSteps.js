/// <reference types="Cypress" />

const { Configurator } = require("../page-objects/configurator/configurator");

Cypress.Commands.add("saveMyCar", () => {
  Configurator.clickOnSaveMyCar()
  .clickLogin()
  .login();
});