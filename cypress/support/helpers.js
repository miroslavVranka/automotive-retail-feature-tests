/// <reference types="Cypress" />

export const Helpers = {
    randomString(string_length) {
        let random_string = "";
        let random_ascii;
        for (let i = 0; i < string_length; i++) {
            random_ascii = Math.floor(Math.random() * 25 + 97);
            random_string += String.fromCharCode(random_ascii);
        }
        return random_string;
    },

    randomNumber(min, max) {
        return Math.random() * (max - min) + min;
    },

    roundUp(num, precision) {
        precision = Math.pow(10, precision);
        return Math.ceil(num * precision) / precision;
    },

    parseNumberFromPrice(input) {
        return parseFloat(input
            .replace(Cypress.env("divideSignCurrent"), Cypress.env("divideSignExpected"))
            .match(/\d+/g).join([]))
    },
    randomItem(items) {
        return items[items.length * Math.random() | 0];
    }
};
