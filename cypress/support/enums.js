export const FilterEnergie = {
  ESSENCE: "ESSENCE",
  DIESEL: "DIESEL",
  HYBRID: "HYBRID",
};
export const FilterGearbox = {
  AUTOMATIQUE: "AUTOMATIQUE",
  MANUELLE: "MANUELLE",
};