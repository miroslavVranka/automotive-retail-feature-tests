export const cash_urls_path = "customData/cash_urls.json";

export const finance_urls_path = "customData/finance_urls.json";

export const journey_offers_part_url = "/configurable";

export const journey_stock_part_url = "/stock";

export const finance_part_url = "/finance";

export const cash_part_url = "/cash";

export const trim_part_url = "/trim";

export const config_part_url = "/selector";

export const login_url = Cypress.env("baseUrl") + "/login";

export const basket_url = "/basket";

export const offers_finance_url =
    Cypress.env("baseUrl") + journey_offers_part_url + finance_part_url;

export const offers_cash_url =
    Cypress.env("baseUrl") + journey_offers_part_url + cash_part_url;

export const stock_finance_url =
    Cypress.env("baseUrl") + journey_stock_part_url + finance_part_url;

export const stock_cash_url =
    Cypress.env("baseUrl") + journey_stock_part_url + cash_part_url;

export const offers_cash_trim_url =
    Cypress.env("baseUrl") +
    trim_part_url +
    journey_offers_part_url +
    cash_part_url +
    Cypress.env("modelNameplate");

export const offers_cash_configurator_url =
    Cypress.env("baseUrl") +
    config_part_url +
    journey_offers_part_url +
    cash_part_url +
    Cypress.env("modelNameplate") +
    Cypress.env("trimNameplate");

export const offers_cash_configurator_refer_url =
    Cypress.env("backofficeApiBaseURL") +
    config_part_url +
    journey_offers_part_url +
    cash_part_url +
    Cypress.env("modelNameplate") +
    Cypress.env("trimNameplate");

export const offers_finance_refer_url =
    Cypress.env("backofficeApiBaseURL") +
    config_part_url +
    journey_offers_part_url +
    finance_part_url

export const offers_finance_trim_refer_url =
    Cypress.env("backofficeApiBaseURL") +
    trim_part_url +
    journey_offers_part_url +
    finance_part_url

export const offers_finance_trim_url =
    Cypress.env("baseUrl") +
    trim_part_url +
    journey_offers_part_url +
    finance_part_url +
    Cypress.env("modelNameplate");

export const offers_finance_configurator_url =
    Cypress.env("baseUrl") +
    config_part_url +
    journey_offers_part_url +
    finance_part_url +
    Cypress.env("modelNameplate") +
    Cypress.env("trimNameplate");

export const request_with_basic_auth =(url) => {
    let matches = url.match(/(https?:\/\/)(.+)/);
    if (matches[1] && matches[2]) {
        return (
            matches[1] +
            "summit" +
            ":" +
            "summit" +
            "@" +
            matches[2]
        );
    }
    return url;
};

export const baseApiUrl = `${Cypress.env("backofficeApiBaseURL")}/spc-api/api/v1/${Cypress.env("COUNTRY").toLowerCase()}/${Cypress.env("language")}/${Cypress.env("BRAND")}`

export const my_details_url = "/my-details";

export const checkout_url = "checkout";

export const fuel_filter = "fuel=";

export const gearbox_filter = "gearbox=";

export const configurator_finance_part_url =
    journey_offers_part_url + finance_part_url;

export const configurator_cash_part_url =
    journey_offers_part_url + cash_part_url;

export const configurated_model_finance =
    Cypress.env("baseUrl") +
    config_part_url +
    journey_offers_part_url +
    finance_part_url +
    Cypress.env("carModel");

export const configurated_model_cash =
    Cypress.env("baseUrl") +
    config_part_url +
    journey_offers_part_url +
    cash_part_url +
    Cypress.env("carModel");
