export const delete_all_faq_url =  "/bo-api/api/faq-categories/delete/all";
export const delete_one_faq_url = "/bo-api/api/faq-category/delete/";
export const create_faq_url = "/bo-api/api/faq-category/create";
export const get_namepalates_url = (brand) => {return `/spc-api/api/v1/fr/fr/${brand}/car-nameplates`};