export const create_mop_endpoint = "/digital-api/api/create-retail-mop";

export const update_mop_dealer = "/digital-api/api/update-mop-dealer/**";

export const update_mop_step = "/digital-api/api/update-mop-step";

export const price_update = "**/car-details-list";

export const car_nameplates = "**/car-nameplates";

export const common_json = "/common.json";

export const filters = "**/filters";

export const px_form = "https://ppr-api-psa-exchange.shakazoola.com/v1/estimate/version/**";

export const px_panel_params = "**/widget-params?**";

