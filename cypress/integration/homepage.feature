Feature: Feature tests for automotive-retail homePage

  Background: Jump into homePage
    Given I jump into offers finance page

  @smoke
  @TC1037
  @RETAIL_FR
  Scenario: TC1037 Homepage - check there are cars and prices
    When journey selector contains "Mensuel"
    Then number of filtered models is greater than 0
    And price of 0 nth model is greater than 1
    And deposit of 0 nth model is greater than 1

  @smoke
  @TC1039.1
  @RETAIL_FR
  Scenario: TC1039.1 Homepage - Basic Filtration - by energie
    When I filter car by 0 nth "Energy"
    Then url contain string "fuel="
    And number of filtered models is greater than 0

  @smoke
  @TC1039.2
  @RETAIL_FR
  Scenario: TC1039.2 Homepage - Basic Filtration - by gearbox
    When I filter car by 0 nth "Gearbox"
    Then url contain string "gearbox="
    And number of filtered models is greater than 0

  @smoke
  @TC1039.3
  @RETAIL_FR
  Scenario: TC1039.3 Homepage - Basic Filtration - by price
    When I filter car by price
      | minPrice | maxPrice |
      | 330      | 803      |
    Then url contain string "prices="
    And number of filtered models is greater than 0

  @smoke
  @TC1045
  @RETAIL_FR
  Scenario: TC1045 Homepage - Open / close finance info
    When number of filtered models is greater than 0
    And open finance info option of 0 nth model from offered options
    Then close finance info

  @smoke
  @TC1046
  @RETAIL_FR
  Scenario: TC1046 Homepage - Switch Finance/Cash journey
    When start server for api validation
      | method | partialUrl      | alias |
      | POST   | /car-nameplates | cars  |
    And switch into different journey
    Then number of filtered models is greater than 0
    And price of 0 nth model is greater than 0
    And legal text should not exists
    And journey selector contains "Comptant"
    And verify resultCode of api with alias "cars"
    And url contain string "/cash"

