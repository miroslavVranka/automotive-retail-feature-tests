Feature: Feature tests for automotive-retail homePage

  Background: Jump into trimpage
    Given I jump into offers finance trim page

  @smoke
  @cash
  @TC1041.1
  @RETAIL_FR
  Scenario:  TC1041.1 TrimPage - Click on first trim offered on TrimPage
    When journey selector contains "Mensuel"
    Then number of filtered models is greater than 0
    And price of 0 nth model is greater than 1
    And deposit of 0 nth model is greater than 1

