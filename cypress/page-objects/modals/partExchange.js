/// <reference types="Cypress" />

import { PartExchangePanel } from "../configurator/partExchangePanel"

const VRM = 'enter-vrm'; 
const KM = 'overallMileage';
const VERSION = 'version_chosen';
const ESTIMATE_PRICE = '.reprise-ferme-value';
const CONTINUE_BTN = '#button-init-immat';
const CONFIRM_BTN = '#button-version';
const SUBMIT_BTN = '#add_to_cart_estimate';
const CAR_MODEL = '.subtitle > span';

export const PartExchange = {
  fillVrmInput(px_vrm) {
    cy.getById(VRM).fillInput(px_vrm);
    return this;
  },

  fillKmInput() {
    cy.getById(KM).fillInput("8000");
    return this;
  },

  selectNthVersion(nth) {
    cy.getById(VERSION)
      .click()
      .find(`[data-option-array-index= ${nth}]`)
      .click();
    return this;
  },

  getEstimatePrice() {
    return cy.get(ESTIMATE_PRICE);
  },

  submitFirstStep() {
    cy.get(CONTINUE_BTN).click();
    return this;
  },

  submitSecondStep() {
    cy.get(CONFIRM_BTN).click();
    return this;
  },

  submitThirdStep() {
    cy.get(SUBMIT_BTN).click();
    return PartExchangePanel;
  },

  getVehicle() {
    return cy.get(CAR_MODEL)
  }
};