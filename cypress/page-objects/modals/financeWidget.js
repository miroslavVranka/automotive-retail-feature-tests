/// <reference types="Cypress" />

const CLOSE_MODAL = '.fipsa-header-close-button';
const FINANCIAL_WIDGET = '.Fipsa';

export const FinanceWidget = {
  
  close() {
    return cy.get(CLOSE_MODAL).click();
  },

  getModalWindow() {
    return cy.get(FINANCIAL_WIDGET);
  }
};