/// <reference types="Cypress" />

const CLOSE_MODAL = 'TESTING_CLOSE_MODAL';
const FINANCIAL_WIDGET = '.modalWindow__wrap';
const PRICE = 'TESTING_FINANCE_PRICE_';

export const FinanceInfo = {
  
  close() {
    cy.getByDataId(CLOSE_MODAL).click();
  },

  getModalWindow() {
    return cy.get(FINANCIAL_WIDGET);
  },

  getStorePriceText() {
    return cy.getByDataId(`${PRICE}${Cypress.env("StorePriceIndex")}`);
  },

  getMonthlyPriceText() {
    return cy.getByDataId(`${PRICE}${Cypress.env("MonthlyPriceIndex")}`);
  },

  getDepositText() {
    return cy.getByDataId(`${PRICE}${Cypress.env("DepositIndex")}`);
  },
  
  getPrice(nth) {
    return cy.get(PRICE).eq(nth)
  }
};