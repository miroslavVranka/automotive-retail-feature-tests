/// <reference types="Cypress" />

import { Prices } from "./prices";

const MODEL_DETAIL = 'button[id^= "TESTING_DETAIL_"]';
const MODEL_NAME = '.nameTitle a';

export const Models = {
  getDetailButton(nth) {
    return cy.get(MODEL_DETAIL).eq(nth);
  },

  select(nth) {
    this.getDetailButton(nth).click({ force: true });
    return this;
  },

  getPrices() {
    return Prices;
  },

  getCount() {
    return cy.get(MODEL_DETAIL).its("length");
  },
  getModelName(nth) {
    return cy.get(MODEL_NAME).eq(nth);
  }
};
