/// <reference types="Cypress" />

import { Prices } from "./prices";

const TRIM_DETAIL = 'button[id^="TESTING_GO_TO_DETAIL_"]';
const COLOR_DETAIL = '[id^="TESTING_GO_TO_DETAIL_COLOR_"]';
const MODEL_NAME = '.content-title';
const MODEL_DETAIL = 'button[id^= "TESTING_GO_TO_DETAIL_"]';

export const Trims = {
  getDetailButton(nth) {
    return cy.get(TRIM_DETAIL).eq(nth);
  },

  select(nth) {
    this.getDetailButton(nth).click();
    return this;
  },

  getPrices() {
    return Prices;
  },

  getCount() {
    return cy.get(TRIM_DETAIL).its("length");
  },

  clickOnColorDetail(nth) {
    cy.get(COLOR_DETAIL)[nth].click();
  },

};
