import { FinanceInfo } from '../modals/financeInfo';

const FINANCE_INFO = 'TESTING_INFOICON';
const TOTAL_PRICE = 'TESTING_TOTAL_PRICE';
const LEGAL_TEXT = 'TESTING_LEGAL';
const STORE_PRICE = 'TESTING_STORE_PRICE';
const PRICE_LABEL = 'TESTING_PRICE_LABEL_';
const LEGAL_MENTIONS = 'TESTING_LEGAL_';

export const Prices = {
  getPrice(nth) {
    return this.getTotalPriceElements().eq(nth).parseFloatFromText();
  },

  getTotalPriceElements() {
    return cy.getByContainsDataId(TOTAL_PRICE);
  },

  getPriceLabelElements() {
    return cy.getByContainsDataId(PRICE_LABEL);
  },

  getLegalTexts() {
    return cy.getByContainsDataId(LEGAL_MENTIONS);
  },

  getLegalText(nth) {
    return this.getLegalTexts().eq(nth);
  },

  getDeposits() {
    return cy.getByContainsDataId(LEGAL_TEXT);
  },

  getDeposit(nth) {
    return this.getDeposits().eq(nth).parseFloatFromText();
  },

  getInfoIconElement(nth) {
    return this.getInfoIconElements().eq(nth);
  },

  getInfoIconElements() {
    return cy.getByContainsDataId(FINANCE_INFO);
  },

  openFinanceInfo(nth) {
    this.getInfoIconElement(nth).click();
    return FinanceInfo;
  },

  getStorePrice(nth) {
    return cy.getByContainsDataId(STORE_PRICE).eq(nth).parseFloatFromText();
  },

  getTotalPriceByExternalId(externalId) {
    return cy.getByDataId(`${TOTAL_PRICE}_${externalId}`).text();
  },

  getStorePriceByExternalId(externalId) {
    return cy.getByDataId(`${STORE_PRICE}_${externalId}`).text();
  },

  getStorePriceElements() {
    return cy.getByContainsDataId(STORE_PRICE);
  },

  getMonthlyPriceElements() {
    return cy.getByContainsDataId(MONTHLY_PRICE);
  },
};
