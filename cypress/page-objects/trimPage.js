/// <reference types="Cypress" />

import {trim_part_url} from "../support/constants/routs"

const GO_BACK_BUTTON = 'TESTING_BACK'

export const trimPage = {
  goBack() {
    return cy.getByDataId(GO_BACK_BUTTON).click()
  }
}
