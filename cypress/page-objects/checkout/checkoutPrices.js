const TOTAL_PRICE = "TESTING_TOTAL_PRICE";
const TOTAL_CASH_PRICE = "TESTING_TOTAL_CASH_PRICE";
const STORE_PRICE = "TESTING_STORE_PRICE";
const BASE_PRICE = "TESTING_BASE_PRICE";
const SUBTOTAL_PRICE = "TESTING_TOTAL_CASH_PRICE";
const OPTION_ITEMS = "TESTING_OPTION_PRICE_";
const COLOR_ITEMS = "TESTING_COLOR_ITEM_";
const EXTERIOR_COLOR_PRICE = "TESTING_EXTERIORCOLOR_PRICE";
const INTERIOR_COLOR_PRICE = "TESTING_INTERIORCOLOR_PRICE";
const FINAL_PRICE = "[data-testid='TESTING_FINAL_PRICE'] > div > div > span > span"

export const CheckoutPrice= {

  getSubTotalPrice() {
    return cy.getByDataId(SUBTOTAL_PRICE);
  },

  getStorePrice() {
    return cy.getByDataId(STORE_PRICE).text();
  },

  getBasePriceText() {
    return cy.getByDataId(BASE_PRICE);
  },

  getOptionItems() {
    return cy.getByContainsDataId(OPTION_ITEMS)
  },

  getColorItems() {
    return cy.getByDataIdWith2Childs(COLOR_ITEMS, "span");
  },

  getTotalCashPrice() {
    return cy.getByContainsDataId(TOTAL_CASH_PRICE).parseFloatFromText();

  },
  
  getExteriorPriceText() {
    return cy.getByDataId(EXTERIOR_COLOR_PRICE);
  },

  getInteriorPriceText() {
    return cy.getByDataId(INTERIOR_COLOR_PRICE);
  },

  getFinalPriceText() {
    return cy.getByDataId(FINAL_PRICE);
  },

  getTotalPriceText() {
    return cy.getByDataId(TOTAL_PRICE);
  },

  getTotalCashPriceText() {
    return cy.getByContainsDataId(TOTAL_CASH_PRICE);

  },
};
