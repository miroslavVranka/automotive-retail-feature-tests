import { CheckoutPrice } from "./checkoutPrices";
import { FinanceInfo } from "../modals/financeInfo";

const CONTINUE_BTN = "TESTING_HANDLE_DEALER";
const DEALER_SELECTOR = "TESTING_DEALER_ITEM_";
const FINANCE_INFO_ACCORDEON = ":nth-child(3) .Collapsible__trigger"
const PX_ACCORDEON = ":nth-child(2) .Collapsible__trigger"

export const Checkout = {

  selectNthDealer(nth) {
    cy.getByDataId(`${DEALER_SELECTOR}${nth}`).click();
    return this;
  },

  clickToContinue() {
    cy.getByDataId(CONTINUE_BTN).click();
    return this;
  },

  openFinanceInfo() {
    cy.get(FINANCE_INFO_ACCORDEON).last().click() // needs data IDs
    return FinanceInfo;
  },

  getPrice() {
    return CheckoutPrice;
  }
};
