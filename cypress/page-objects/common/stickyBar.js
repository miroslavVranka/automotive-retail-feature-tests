/// <reference types="Cypress" />

import { Prices } from "../cars/prices";
import { FinanceInfo } from "../modals/financeInfo";

const PRICE_SWITCHER = "TESTING_JOURNEY_TYPE";
const BUY_BUTTON = "TESTING_CONTINUE";
const TOTAL_PRICE = "TESTING_PANEL_TOTAL_PRICE";
const FINANCE_INFO_LINK = "TESTING_PANEL_INFOICON";

export const StickyBar = {
  getTotalPrice() {
    return cy.getByDataId(TOTAL_PRICE);
  },

  getFinanceInfoLink() {
    return cy.getByDataId(FINANCE_INFO_LINK);
  },

  switchPrice() {
    return cy.get(PRICE_SWITCHER).click();
  },

  getContinueButton() {
    return cy.getByDataId(BUY_BUTTON);
  },

  clickToContinue() {
    return this.getContinueButton().click();
  },

  getPrice() {
    return Prices;
  },

  openFinanceInfo() {
    this.getFinanceInfoLink().click()
    return FinanceInfo;
  },
}
