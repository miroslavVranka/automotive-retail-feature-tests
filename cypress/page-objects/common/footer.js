/// <reference types="Cypress" />

const ORDER_NOW_SECTION = '.orderNowSection';
const FOOTER = '.footer';
const ARROW_UP = '#TESTING_FOOTER_ARROW_UP';
const TOKY_CONTAINER = '#toky_container';
const footerLinksSelector = 'a[id^=TESTING_FOOTER_] ';
const FAQ_CONTAINER = '#faqs';
export const Footer = {
         getFaqContainer() {
           return cy.get(FAQ_CONTAINER);
         },
         getOrderNowSection() {
           return cy.get(ORDER_NOW_SECTION);
         },
         getFooter() {
           return cy.get(FOOTER);
         },
         getArrowUp() {
           return cy.get(ARROW_UP);
         },
         getTokyWoky() {
           return cy.get(TOKY_CONTAINER);
         },
        
         testAndPingFooterLinks() {
           
         },

         getAllFooterLinks() {
           return cy.get(footerLinksSelector);
         },
       };
