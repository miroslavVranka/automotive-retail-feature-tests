/// <reference types="Cypress" />

const GO_BACK_BUTTON = "TESTING_BACK";
const BRAND_STORE_TITLE = ".title-primary";
const ACCOUNT_TITLE = "TESTING_MY_ACC";
const ACCOUNT_TEXT = "MON COMPTE";
const BASKET_TITLE = "TESTING_BASKET";
const BASKET_TEXT = "MON PANIER";
const BASKET_ICON = "TESTING_BASKET";
const LOGO_ICON = "TESTING_LOGO";
const ACCOUNT_ICON = "TESTING_MY_ACC";

export const Header = {
  goBack() {
    return cy.getByDataId(GO_BACK_BUTTON).click();
  },
  getBrandTitle() {
    return cy.get(BRAND_STORE_TITLE);
  },
  getLogoLink() {
    return cy.getById(LOGO_ICON);
  },

  getMyAccountLink() {
    return cy.getById(ACCOUNT_ICON);
  },

  getBasketLink() {
    return cy.getById(BASKET_ICON);
  },

  clickMyAccountLink() {
    this.getMyAccountLink().click();
  },

  clickBasketLink() {
    this.getBasketLink().click();
  },

  clickLogoLink() {
    this.getLogoLink().click();
  },

      
};
