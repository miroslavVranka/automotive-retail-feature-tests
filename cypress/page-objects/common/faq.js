/// <reference types="Cypress" />

const faqContainer = "#faqs";
const faqItem = ".faqHeader";
const categoryContent = ".categoryContent";
const faqCollapsable = ".Collapsible";
const faqHeader = ".Collapsible > span:first";

export const Faq = {

  verifyLinks(link, subLinks) {
    this.verifyLink(link);
    this.openLink(link);
    subLinks.forEach((subLink) => {
      this.verifyLink(subLink);
    });
  },

  verifyLink(link) {
    cy.get(faqItem).should("contain", link).and("be.visible");
  },

  verifyLinkIsNotPresent(link) {
    cy.get(faqItem).contains(link).should("not.exist");
  },

  verifyTextInLink(link, text) {
    this.openLink(link);
    cy.get(categoryContent).contains(text);
  },

//   testFAQIsClickable() {
//     const links = this.getAllFAQLinks();
//     links.each(($el, index) => {
//       this.clickFAQ(index).should("be.visible");
//       this.closeFAQ(index).should("not.be.visible");
//     });
//   },

  openLink(link) {
    cy.contains(faqItem, link).click();
  },

  clickFAQ(index) {
    const selector = this.getFaqSelectorByIndex(index);
    cy.get(`${selector} ${faqHeader}`)
      .click()
      .get(`${selector}>${faqCollapsable}`);
  },

  getFaqSelectorByIndex(index) {
    return `#TESTING_FAQS_${index}`;
  },

  getAllLinks() {
    return cy.get(faqContainer).children();
  },

  getAllFooterLinks() {
    return cy.get(footerLinksSelector);
  },
};
