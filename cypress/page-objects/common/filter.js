/// <reference types="Cypress" />

import { FilterEnergie, FilterGearbox } from "../../support/enums.js";
import { car_nameplates, price_update } from "../../support/constants/endpoints"

const JOURNEY_SELECTOR = "TESTING_JOURNEY_TYPE";
const JOURNEY_TOGGLE_BTN = "span.toggleOption.toggled";
const FILTER_ENERGY_ESSENCE = "TESTING_ESSENCE";
const FILTER_ENERGY_DIESEL = "TESTING_DIESEL";
const FILTER_ENERGY_HYBRID = "TESTING_HYBRID";
const FILTER_GEARBOX_AUTOMATIC = "TESTING_AUTOMATIC";
const FILTER_GEARBOX_MAN = "TESTING_MAN";
const FILTERS_ENERGY = '[data-testid="TESTING_FILTER_fuel"] button';
const FILTERS_GEARBOX = '[data-testid="TESTING_FILTER_gearbox"] button';
const FILTER_RESET = "TESTING_RESET";
const MIN_PRICE_TEXT = "TESTING_START_RANGE";
const MAX_PRICE_TEXT = "TESTING_END_RANGE";

export const Filter = {
  clickOnReset() {
    return cy.getById(FILTER_RESET).click();
  },

  verifyVisibilityOfJourneySelector() {
    cy.getById(JOURNEY_SELECTOR).should("be.visible");
  },

  clickOnJourneySelector() {
    cy.getById(JOURNEY_SELECTOR).click();
    return this;
  },

  filterByEnergy(filter) {
    switch (filter) {
      case FilterEnergie.ESSENCE:
        cy.getById(FILTER_ENERGY_ESSENCE).click();
        break;
      case FilterEnergie.DIESEL:
        cy.getById(FILTER_ENERGY_DIESEL).click();
        break;
      case FilterEnergie.HYBRID:
        cy.getById(FILTER_ENERGY_HYBRID).click();
        break;
      default:
        break;
    }
  },

  filterByGearbox(filter) {
    switch (filter) {
      case FilterGearbox.AUTOMATIQUE:
        cy.getById(FILTER_GEARBOX_AUTOMATIC).click();
        break;
      case FilterGearbox.MANUELLE:
        cy.getById(FILTER_GEARBOX_MAN).click();
        break;
    }
  },

  getEnergyFitlerButton(nth) {
    return cy.get(FILTERS_ENERGY).eq(nth);
  },

  getGearboxFitlerButton(nth) {
    return cy.get(FILTERS_GEARBOX).eq(nth);
  },

  filterByAnyGearbox(nth) {
    cy.server();
    cy.route("POST", car_nameplates).as("cars");
    this.getGearboxFitlerButton(nth).click();
  //  cy.wait("@cars");
    return this.getGearboxFitlerButton(nth);
  },

  filterByAnyEnergy(nth) {
    cy.server();
    cy.route("POST", car_nameplates).as("cars");
    this.getEnergyFitlerButton(nth).click();
   // cy.wait("@cars");
    return this.getEnergyFitlerButton(nth);
  },

  getSelectedJourney() {
    return cy.get(JOURNEY_TOGGLE_BTN);
  },

  getMinPriceNumber() {
    return cy.getByDataId(MIN_PRICE_TEXT).parseFloatFromText()
  },

  getMaxPriceNumber() {
    return cy.getByDataId(MAX_PRICE_TEXT).parseFloatFromText()
  },
};
