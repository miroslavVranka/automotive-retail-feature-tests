/// <reference types="Cypress" />

const RETURN_BUTTON = "a.topnavbar-returnlink";

export const TopNavBar = {
  clickToReturn() {
    return cy.get(RETURN_BUTTON).click();
  },
};
