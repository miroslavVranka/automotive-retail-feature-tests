
const OPTIONS = 'TESTING_OPTION_';
const SELECTED_OPTION = "div[class^='option-card__image  is-selected']";
const OPTION_PRICES = "TESTING_OPTIONS_PRICE_";
const SELECTED_PRICE = "TESTING_OPTIONS_PRICE";
const CONFIRM_BTN = ".selected-option__add-btn";

export const OptionsPanel = {
  getSelectedOptionClass() {
    return SELECTED_OPTION;
  },

  getIconDivForSelectedOptionElement(el) {
    return el.find(this.getSelectedOptionClass());
  },

  getAllOptions() {
    return cy.getByContainsId(OPTIONS);
  },

  getNotSelectedOption(nth) {
    return this.getAllOptions().not(this.getSelectedOptionClass()).eq(nth);
  },

  clickNotSelectedOption(nth) {
    return this.getNotSelectedOption(nth).click();
  },

  getPrices() {
    return cy.getByContainsDataId(OPTION_PRICES).filter(':contains("+")');
  },

  getSelectedPrice() {
    return cy.getByDataId(SELECTED_PRICE);
  },

  clickToAdd() {
    return cy.get(CONFIRM_BTN).click();
  }
};
