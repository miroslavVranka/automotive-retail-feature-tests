/// <reference types="Cypress" />
import { Authentication } from "../account/authentication";
import { StickyBar } from "../common/stickyBar";
import { FinancePanel } from "../configurator/financePanel";
import { ColorPanel } from "./colorPanel";
import { EnginePanel } from "./enginePanel";
import { InteriorPanel } from "./interiorPanel";
import { OptionsPanel } from "./optionsPanel";
import {PartExchangePanel} from "../configurator/partExchangePanel";

const COLOR_PANEL = "TESTING_CONFIG_COLOR";
const ENGINE_PANEL = "TESTING_CONFIG_MOTO";
const INTERIOR_PANEL = "TESTING_CONFIG_INTER";
const OPTION_PANEL = "TESTING_CONFIG_OPTS";
const CAR_NAME = "TESTING_CARNAME";
const RETRUN_BTN = ".topnavbar-returnlink";
const MY_SAVES = "TESTING_SAVE_CAR_BOX";
const BUY_BUTTON = "TESTING_TO_BASKET_BOX";
const ANY_PANEL = "TESTING_CONFIG_";

export const Configurator = {
         clickOnSaveMyCar() {
           this.getSaveMyCarButton().click();
           return Authentication;
         },

         getStickyBar() {
           return StickyBar;
         },

         getSaveMyCarButton() {
           return cy.getById(MY_SAVES);
         },

         getFinancePanel() {
           return FinancePanel;
         },

         getColorPanel() {
           return ColorPanel;
         },

         getEnginePanel() {
           return EnginePanel;
         },

         getInteriorPanel() {
           return InteriorPanel;
         },

         getOptionsPanel() {
           return OptionsPanel;
         },

         getPXPanel() {
           return PartExchangePanel;
         },
         
         clickToBuy() {
           cy.getById(BUY_BUTTON).click();
         },

         clickToReturn() {
           cy.get(RETRUN_BTN).click();
         },

         getCarName() {
           return cy.getByDataId(CAR_NAME).text();
         },

         getColorPanelSelector() {
           return cy.getById(COLOR_PANEL);
         },

         clickToColorPanel() {
           this.getColorPanelSelector().click();
           return ColorPanel;
         },

         clickToEnginePanel() {
           this.getEnginePanelSelector().click();
           return EnginePanel;
         },

         getEnginePanelSelector() {
           return cy.getById(ENGINE_PANEL);
         },

         clickToInteriorPanel() {
           this.getInteriorPanelSelector().click();
           return InteriorPanel;
         },

         getInteriorPanelSelector() {
           return cy.getById(INTERIOR_PANEL);
         },

         clickToOptionsPanel() {
           this.getOptionsPanelSelector().click();
           return OptionsPanel;
         },

         getOptionsPanelSelector() {
           return cy.getById(OPTION_PANEL);
         },

         getAllPanels() {
           return cy.getByContainsId(ANY_PANEL);
         }
       };
