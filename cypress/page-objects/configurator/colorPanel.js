const COLORS = '[id^="TESTING_COLOR_"]>div';
const SELECTED_COLOR = "[class='color-button is-selected']";
const COLOR_PRICE = 'TESTING_COLOR_PRICE';
export const ColorPanel = {

  getSelectedPrice(){
    return cy.getByContainsDataId(COLOR_PRICE);
  },

  getSelectedColorClass() {
    return SELECTED_COLOR;
  },

  getIconDivForSelectedColorElement(el) {
    return el.find(this.getSelectedColorClass());
  },

  getAllColors() {
    return cy.get(COLORS);
  },

  getNotSelectedColor(nth) {
    return this.getAllColors().not(SELECTED_COLOR).eq(nth);
  },

  clickNotSelectedColor(nth) {
    return this.getNotSelectedColor(nth).click();
  },
};
