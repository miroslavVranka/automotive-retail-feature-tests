const DELETE_ICON = "TESTING_PX_DELETE";
const PX_PRICE = ".pxEstimationPrice span:nth-child(2)";
const ADD_PX = "widgetView";
const VRM = "TESTING_REG_NUM";
const MARK = "TESTING_MARK";
const MODEL = "TESTING_MODEL";

export const PartExchangePanel = {
  getDeleteBtn() {
    return cy.getByDataId(DELETE_ICON);
  },

  deletePX() {
    return this.getDeleteBtn().click();
  },

  getPxPrice() {
    return cy.get(PX_PRICE);
  },

  getPartExchangeBtn() {
    return cy.getById(ADD_PX);
  },

  clickOnPartExchangeBtn() {
    return this.getPartExchangeBtn().click();
  },

  getVRM() {
    return cy.getByDataId(VRM);
  },

  getMark() {
    return cy.getByDataId(MARK);
  },

  getModel() {
    return cy.getByDataId(MODEL);
  }
};
