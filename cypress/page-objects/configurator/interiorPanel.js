const INTERIORS = "TESTING_INTERIOR_";
const SELECTED_INTERIOR = ".interiorColor-isSelected";
const INTERIOR_SELECTED_PRICE = "TESTING_INTERIORCOLOR_PRICE";
const INTERIOR_PRICES = "TESTING_INTERIORCOLOR_ITEM_PRICE";
const INTERIOR_ITEM_PRICE = "[data-testid^='TESTING_INTERIORCOLOR_ITEM_PRICE_']";
const INTERIOR_ITEM_TITLE = "[data-testid^='TESTING_INTERIORCOLOR_ITEM_TITLE_']";
const INTERIOR_ITEM = "[data-testid^='TESTING_INTERIORCOLOR_ITEM_']";
const INTERIOR_DESC = "TESTING_INTERIORCOLOR_DESCR";
const INTERIOR_LEAD_TIME = "TESTING_INTERIORCOLOR_LEAD_TIME";

export const InteriorPanel = {
         getSelectedInterior() {
           return SELECTED_INTERIOR;
         },

         getAllInteriors() {
           return cy.getByContainsId(INTERIORS);
         },

         getNotSelectedInterior(nth) {
           return this.getAllInteriors()
             .not(this.getSelectedInterior())
             .eq(nth);
         },

         clickNotSelectedInterior(nth) {
           return this.getNotSelectedInterior(nth).click();
         },

         getSelectedPrice() {
           return cy.getByDataId(INTERIOR_SELECTED_PRICE);
         },

         getSelectedInteriorPrice() {
           return cy
             .get(SELECTED_INTERIOR)
             .children(INTERIOR_ITEM)
             .children(INTERIOR_ITEM_PRICE);
         },

         getSelectedInteriorItemTitle() {
           return cy
             .get(SELECTED_INTERIOR)
             .children(INTERIOR_ITEM)
             .children(INTERIOR_ITEM_TITLE)
             .text();
         },

         getSelectedInteriorTitle() {
           return cy.getByDataId(INTERIOR_DESC).text();
         },

         getLeadTimeValue() {
           return cy.getByDataId(INTERIOR_LEAD_TIME).parseFloatFromText();
         },

         getSelectedInteriorPrice() {
           return this.getSelectedPrice().parseFloatFromText();
         },

         getPrices() {
           return cy
             .getByContainsDataId(INTERIOR_PRICES)
             .filter(':contains("+")');
         },
       };
