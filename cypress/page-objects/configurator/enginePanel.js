const ENGINES = "TESTING_MOTOR_";
const SELECTED_ENGINE = "div[class^='motorization motorization-isSelected']";
const ENGINE_PRICES = "TESTING_MOTOR_PRICE_";
const SELECTED_ENGINE_PRICE = "TESTING_MOTOR_PRICE";

export const EnginePanel = {
  getSelectedEngineClass() {
    return SELECTED_ENGINE;
  },

  getIconDivForSelectedEngineElement(el) {
    return el.find(this.getSelectedEngineClass());
  },

  getAllEngines() {
    return cy.getByContainsId(ENGINES);
  },

  getNotSelectedEngine(nth) {
    return this.getAllEngines().not(this.getSelectedEngineClass()).eq(nth);
  },

  clickNotSelectedEngine(nth) {
    return this.getNotSelectedEngine(nth).click();
  },

  getPrices() {
    return cy.getByContainsDataId(ENGINE_PRICES);
  },

  getSelectedPrice() {
    return cy.getByDataId(SELECTED_ENGINE_PRICE);
  },
};
