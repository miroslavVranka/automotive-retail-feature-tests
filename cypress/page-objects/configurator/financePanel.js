/// <reference types="Cypress" />
import { FinanceWidget } from '../modals/financeWidget';

const FINANCE_PANEL_HEADER = 'TESTING_PRICE_PANEL';
const LEGAL_MENTIONS = 'TESTING_PRICE_LEGAL_TEXT';
const PERSONALIZE_FINANCING_BUTTON = 'TESTING_FINANCE_WIDGET_LINK';
const TOTAL_PRICE = 'TESTING_TOTAL_PRICE';
const STORE_PRICE = "[data-testid='TESTING_STORE_PRICE'] > span";
const FINANCE_INFO_ICON = "TESTING_INFOICON";

export const FinancePanel = {
    getPanelHeader() {
        return cy.getByDataId(FINANCE_PANEL_HEADER).find('span.Collapsible__trigger')
    },

    clickToPanelHeader() {
        this.getPanelHeader().click();
    },

    getLegalMentions() {
        return cy.getByDataId(LEGAL_MENTIONS);
    },

    openFinaceWidget() {
        cy.getByDataId(PERSONALIZE_FINANCING_BUTTON).click()
        return FinanceWidget;
    },

    getTotalPrice() {
        return cy.getByDataId(TOTAL_PRICE);
    },

    getPrice() {
        return this.getTotalPrice().parseFloatFromText();
    },

    getFinanceInfoIcon() {
        return cy.getByDataId(FINANCE_INFO_ICON);
    },

    getStorePrice() {
        return cy.get(STORE_PRICE).text();
    }
};
