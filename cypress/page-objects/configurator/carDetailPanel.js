const CAR_DETAIL_PANEL = "carInfoBox";
const MOBILE_ICON_OPEN = "[id='carInfoBox'] svg";
const MOBILE_OPEN = "TESTING_CARINFO_OPEN";
const MOBILE_ICON_CLOSE = "TESTING_CARINFO_CLOSE";
const CAR_DETAIL_TEXT = "TESTING_CARINFO_TEXT";
const CAR_DETAIL_TECH = "TESTING_CARINFO_TECH";

export const CarDetailPanel = {
  getPanel() {
    return cy.getById(CAR_DETAIL_PANEL)
  },

  getText() {
    return cy.getByDataId(CAR_DETAIL_TEXT)
  },

  getTech() {
    return cy.getByDataId(CAR_DETAIL_TECH);
  }
};

export const MobileCarDetailPanel = {
  clickToIconOpen() {
    cy.get(MOBILE_ICON_OPEN).click();
  },

  clickToOpen() {
    cy.getByDataId(MOBILE_OPEN).click()
  },

  clickToIconClose() {
    cy.getByDataId(MOBILE_ICON_CLOSE).click();
  }
};
