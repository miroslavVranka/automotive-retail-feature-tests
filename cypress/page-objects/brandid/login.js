/// <reference types="Cypress" />

const EMAIL = 'email';
const PASSWORD = 'password';
const SUBMIT = 'submit';

export const Login = {
  login() {
    cy.getById(EMAIL).fillInput(Cypress.env("brandidEmail"));
    cy.getById(PASSWORD).fillInput(Cypress.env("brandidPassword"));
    cy.getById(SUBMIT).click();
  },
};