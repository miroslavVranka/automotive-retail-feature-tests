/// <reference types="Cypress" />

import { Login } from "../brandid/login";

const LOGIN_BTN = 'TESTING_LOGIN';

export const Authentication = {
    
    clickLogin() {
        cy.getByDataId(LOGIN_BTN).click();
        return Login;
    }
}