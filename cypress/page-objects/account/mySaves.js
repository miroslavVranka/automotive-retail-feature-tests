/// <reference types="Cypress" />

const PRICE = '.CardItem_bigPrice__U7qKO';  // TODO data-testid is needed
const CONTINUE = '.ContinueWithThisCar_continueButton__-_QuL' // TODO data-testid is needed
const CAR_NAME = 'CardItem_carModel__34-CP' // TODO data-testid is needed

export const MySaves = {

    getPrice(nth) {
        return cy.get(PRICE).eq(nth).parseFloatFromText()
    },

    goToBasket(nth) {
        cy.get(CONTINUE).eq(nth).click()
    },

    getCarName(nth) {
        cy.get(CAR_NAME).eq(nth)
    }
};

