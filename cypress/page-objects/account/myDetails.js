/// <reference types="Cypress" />

const EDIT_INFO_BTN = 'userDetails';
const FIRST_NAME = '.firstname';  //TODO data-testid is needed

export const MyDetails = {

    clickOnEdit() {
        cy.getById(EDIT_INFO_BTN).click()
        return this;
    },

    changeFirstName(firstName) {
        this.clickOnEdit();
        cy.get(FIRST_NAME).fillInput(firstName)
        return this;
    }
}

