import { StickyBar } from "../common/stickyBar";
import { Recapitulation } from "./basketRecapitulation";
import { FinancePanel } from "./basketFinancePanel";
import { PartExchangePanel } from "../configurator/partExchangePanel";

const CAR_NAME = "TESTING_CARNAME";
const ENGINE_NAME = "TESTING_ENGINENAME";
const BUY_BUTTON = "TESTING_TO_BASKET_BOX";
const SAVE_CAR_BUTTON = "TESTING_SAVE_CAR_BOX";

export const Basket = {

  getCarName() {
    return cy.getByDataId(CAR_NAME);
  },
  
  getEngineName() {
    return cy.getByDataId(ENGINE_NAME);
  },

  getPrice() {
    return Recapitulation;
  },

  getStickyBar() {
    return StickyBar;
  },

  clickToBuy() {
    return cy.getById(BUY_BUTTON).click()
  },

  getFinancePanel() {
    return FinancePanel;
  },

  getPXPanel() {
    return PartExchangePanel;
  }
};
