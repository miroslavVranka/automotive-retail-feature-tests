const EMPTY_BASKET_BUTTON = "TESTING_EMPTY_BASKET";
const EMPTY_BASKET_MESSAGE = "TESTING_EMPTY_BASKET_TEXT";
const LATEST_OFFER_CARD = "TESTING_HOMEPAGE_OPTION_CARD";

export const EmptyBasket = {
  chooseYourVehicle() {
    cy.getByDataId(EMPTY_BASKET_BUTTON).click();
    return this;
  },

  getEmptyBasketMessage() {
    return cy.getByDataId(EMPTY_BASKET_MESSAGE);
  },

  getLatestOfferCard() {
    return cy.getByDataId(LATEST_OFFER_CARD);
  },
};
