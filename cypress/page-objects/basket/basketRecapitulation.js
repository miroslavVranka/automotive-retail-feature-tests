const TOTAL_PRICE = "TESTING_TOTAL_PRICE";
const TOTAL_CASH_PRICE = "TESTING_TOTAL_CASH_PRICE";
const STORE_PRICE = "TESTING_STORE_PRICE";
const BASE_PRICE = "TESTING_BASE_PRICE";
const OPTIONS_SUBTOTAL_PRICE = "TESTING_OPTIONS_SUBTOTAL";
const OPTION_ITEM = "TESTING_OPTION_ITEM_";
const COLOR_ITEMS = "TESTING_COLOR_ITEM_";
const DEPOSIT_PRICE = "TESTING_DEPOSIT_PRICE";
const OPTIONS_TOTAL_PRICE = "TESTING_TOTAL_OPTIONS_PRICE"

export const Recapitulation = {

  getTotalPriceText() {
    return cy.getByDataId(TOTAL_PRICE);
  },

  getTotalCashPriceText() {
    return cy.getByDataId(TOTAL_CASH_PRICE);
  },

  getSubTotalPriceText() {
    return cy.getByDataId(OPTIONS_SUBTOTAL_PRICE);
  },

  getSubTotalPrice() {
    return cy.getByDataIdWith2Childs(OPTIONS_SUBTOTAL_PRICE, "span").text();
  },

  getStorePrice() {
    return cy.getByDataId(STORE_PRICE);
  },

  getBasePriceText() {
    return cy.getByDataId(BASE_PRICE);
  },

  getBasePrice() {
    return cy.getByDataIdWith2Childs(BASE_PRICE, "span").text();
  },

  getNthOptionPrice(nth) {
    return cy.getByDataIdWith2Childs(`${OPTION_ITEM}${nth}`, "span");
  },

  getColorItems() {
    return cy.getByDataIdWith2Childs(COLOR_ITEMS, "span");
  },

  getCashPrice() {
    return cy.getByContainsDataId(TOTAL_CASH_PRICE).parseFloatFromText();
  },

  getDepositText() {
    return cy.getByDataId(DEPOSIT_PRICE)
  },

  getTotalOptionsPriceText() {
    return cy.getByDataId(OPTIONS_TOTAL_PRICE);
  },


};
