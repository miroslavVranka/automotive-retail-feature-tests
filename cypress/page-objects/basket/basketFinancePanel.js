import { FinanceInfo } from "../modals/financeInfo";

const TOTAL_PRICE = "TESTING_TOTAL_PRICE";
const TOTAL_CASH_PRICE = "TESTING_TOTAL_CASH_PRICE";
const FINANCE_INFO_BTN = "TESTING_PAYMENT_DETAILS";
const FINANCE_WIDGET_BTN = "TESTING_FINANCE_WIDGET";


export const FinancePanel= {

  getTotalPriceText() {
    return cy.getByDataId(TOTAL_PRICE);
  },

  getTotalCashPriceText() {
    return cy.getByDataId(TOTAL_CASH_PRICE);
  },

  getFinanceInfoBtn() {
    return cy.getByDataId(FINANCE_INFO_BTN)
  },
  openFinanceInfo() {
    this.getFinanceInfoBtn().click()
    return FinanceInfo;
  }
};
