const TOTAL_PRICE = "TESTING_TOTAL_PRICE";
const TOTAL_CASH_PRICE = "TESTING_TOTAL_CASH_PRICE";
const STORE_PRICE = "TESTING_STORE_PRICE";
const BASE_PRICE = "TESTING_BASE_PRICE";
const OPTIONS_SUBTOTAL_PRICE = "TESTING_OPTIONS_SUBTOTAL"
const OPTION_ITEMS = "TESTING_OPTION_ITEM_";
const COLOR_ITEMS = "TESTING_COLOR_ITEM_";

export const BasketPrice= {

  getTotalPrice() {
    return cy.getByDataId(TOTAL_PRICE);
  },

  getTotalCashPrice() {
    return cy.getByDataId(TOTAL_CASH_PRICE).text();
  },

  getOptionsSubTotalPrice() {
    return cy.getByDataIdWith2Childs(OPTIONS_SUBTOTAL_PRICE, "span").text();
  },

  getStorePrice() {
    return cy.getByDataId(STORE_PRICE).text();
  },

  getBasePrice() {
    return cy.getByDataIdWith2Childs(BASE_PRICE, "span").text();
  },

  getOptionItems() {
    return cy.getByDataIdWith2Childs(OPTION_ITEMS, "span")
  },

  getColorItems() {
    return cy.getByDataIdWith2Childs(COLOR_ITEMS, "span");
  },

  getCashPrice() {
    return cy.getByContainsDataId(TOTAL_CASH_PRICE).parseFloatFromText();
  }
};
